"""
Views for report API.
"""
from drf_spectacular.utils import (
    extend_schema_view,
    extend_schema,
    OpenApiParameter,
    OpenApiTypes,
)

from rest_framework import (
    mixins,
    viewsets,
    status,
)
from rest_framework.views import APIView
from django.shortcuts import render
from .generate_reports import get_reports, get_report_summary


from rest_framework.decorators import action, api_view
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from core.models import (
    GroundsDrivewaySummary, GarageAutomaticOpenerSummary,
    GarageElectricalSummary, GarageExteriorDoorSummary, GroundsCoversSummary,
    GroundsDeckSummary, GroundsFenceSummary, GroundsHoseBibsSummary,
    GroundsLandscapingSummary, GroundsPatioSummary,
    GroundsRetainingWallSummary, LaundryPhotos,
    GroundsSidewalkSummary, GroundsStoopStepsSummary, ExteriorCaulkingSummary,
    ExteriorChimneySummary, ExteriorDoorsSummary, ExteriorFasciaSummary,
    ExteriorFlashingSummary, ExteriorGuttersSummary, ExteriorSidingSummary,
    ExteriorSlabOnFoundationSummary, ExteriorSoffitSummary,
    ExteriorTrimSummary, DiningRoomPhotos, ReportDetailsPhoto,
    ExteriorWindowsSummary, RoofConditionSummary, RoofFlashingSummary,
    RoofGeneralSummary, RoofPlumbingVentsSummary, RoofSkylightsSummary,
    RoofStyleSummary, RoofValleysSummary, RoofVentilationSummary,
    BasementColumnsSummary, BasementDrainageSummary, BasementFloorSummary,
    BasementFoundationSummary, BasementGirdersBeamsSummary,
    BasementJoistsSummary, LivingRoomPhotos,
    BasementSeismicBoltsSummary, InteriorAtticSummary,
    BasementStairsSummary, BasementSubfloorSummary,
    GarageFireSeperationSummary, GarageFloorSummary, GarageGuttersSummary,
    GarageOverheadDoorSummary, GarageSafetyReverseSummary,
    GarageSidingSummary, GarageSillPlatesSummary,
    GarageTypeSummary, KitchenAppliancesSummary, KitchenCabinetsSummary,
    KitchenCountertopSummary, KitchenFloorSummary, KitchenPlumbingSummary,
    KitchenWallsCeilingSummary, BathroomElectricalSummary,
    BathroomPlumbingSummary, InteriorSmokeCarbonDetSummary,
    BedroomSummary, PlumbingGasShutoffSummary, InteriorStepsSummary,
    PlumbingWaterHeaterSummary, PlumbingWaterServiceSummary,
    ElectricalMainPanelSummary, InteriorFireplaceSummary,
    ElectricalSubPanelSummary, GroundsPorchSummary, CrawlspaceCrawlSummary,
    CrawlspaceAccessSummary, CrawlspaceInsulationSummary,
    CrawlspaceVaporBarrierSummary, ExteriorServiceEntrySummary,
    CrawlspaceVentilationSummary, ExteriorWallConstructionSummary,
    ReportDetails, Grounds, Roof, Exterior, HeatingSystem,
    GarageCarport, Kitchen, Basement, Crawlspace,
    Plumbing, ElectricalCoolingSystems, DiningRoom, Laundry,
    LivingRoom, Interior, Bathroom,
    Bedrooms, GarageAutomaticOpenerPhotos, GarageExteriorDoorPhotos,
    GarageElectricalPhotos, GarageFireSeperationPhotos,
    GarageFloorPhotos, GarageGuttersPhotos, GarageOverheadDoorPhotos,
    GarageRoofingPhotos, GarageSafetyReversePhotos,
    GarageSidingPhotos, GarageSillPlatesPhotos, GarageTrimPhotos,
    GarageTypePhotos, GroundsCoversPhotos, GroundsDeckPhotos,
    GroundsDrivewayPhotos, GroundsFencePhotos, GroundsHoseBibsPhotos,
    GroundsLandscapingPhotos, GroundsPatioPhotos, GroundsPorchPhotos,
    GroundsRetainingWallPhotos, GroundsSideWalksPhotos,
    GroundsStoopStepsPhotos, ElectricalMainPanelPhotos,
    EvaporatorCoilPhotos, ElectricalSubPanelPhotos, ExteriorCaulkingPhotos,
    ExteriorDoorsPhotos, ExteriorElectricalPhotos,
    ExteriorCoolingUnitPhotos, ExteriorFasciaPhotos, ExteriorSidingPhotos,
    ExteriorSoffitPhotos, ExteriorFlashingPhotos, ExteriorTrimPhotos,
    ExteriorGuttersPhotos, BasementColumnsPhotos, BasementFloorPhotos,
    BasementDrainagePhotos, BasementGirdersBeamsPhotos,
    BasementStairsPhotos, BasementSeismicBoltsPhotos, BedroomPhotos,
    BasementFoundationPhotos, BasementJoistsPhotos, BasementSubfloorPhotos,
    BathroomElectricalPhotos, BathroomPlumbingPhotos, BoilerPhotos,
    PlumbingGasShutoffPhotos, PlumbingWaterHeaterPhotos,
    KitchenAppliancesPhotos, PlumbingWaterServicePhotos, KitchenFloorPhotos,
    KitchenCountertopPhotos, KitchenCabinetsPhotos, KitchenPlumbingPhotos,
    KitchenWallsCeilingPhotos, InteriorAtticPhotos,
    CrawlspaceAccessPhotos, CrawlspaceCrawlPhotos, CrawlspaceFloorPhotos,
    CrawlspaceDrainagePhotos, CrawlspaceFoundationPhotos,
    CrawlspaceJoistsPhotos, CrawlspaceInsulationPhotos,
    CrawlspaceGirdersBeamsPhotos, CrawlspaceSeismicBoltsPhotos,
    CrawlspaceSubfloorPhotos, CrawlspaceVaporBarrierPhotos,
    CrawlspaceVentilationPhotos, RoofConditionPhotos, RoofFlashingPhotos,
    RoofGeneralPhotos, RoofPlumbingVentsPhotos, RoofSkylightsPhotos,
    RoofStylePhotos, RoofValleysPhotos, RoofVentilationPhotos,
    InteriorFireplacePhotos, InteriorSmokeCarbonDetPhotos,
    InteriorStepsPhotos, ExteriorChimneyPhotos, ExteriorWindowsPhotos,
    ExteriorSlabOnGradePhotos, HeatingOtherSystemsPhotos,
    FurnacePhotos, PlumbingWellPumpPhotos, PlumbingSanitaryPumpPhotos,
    Overview, ReceiptInvoice, Summary, PhotoAndComments,
    UserCommentsSummary, GenerateReport
)
from report import serializers
from django.http import JsonResponse, HttpResponse
from django.template.loader import render_to_string
from weasyprint import HTML # type: ignore


class ReportDetailsViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.ReportDetailsSerializer
    queryset = ReportDetails.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset
        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.ReportDetailsSerializer
        elif self.action == 'report-details-photo':
            return serializers.ReportDetailsPhotoSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new report."""
        serializer.save(user=self.request.user)


class ReportDetailsPhotoViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ReportDetailsPhotoSerializer
    queryset = ReportDetailsPhoto.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_report_details_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'report_details_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class OverviewSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.OverviewSerializer
    queryset = Overview.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.OverviewSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class ReceiptInvoiceSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.ReceiptInvoiceSerializer
    queryset = ReceiptInvoice.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.ReceiptInvoiceSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class SummarySectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.SummarySerializer
    queryset = Summary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.SummarySerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class GroundsSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.GroundsSerializer
    queryset = Grounds.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.GroundsSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class RoofSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.RoofSerializer
    queryset = Roof.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.RoofSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class ExteriorSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.ExteriorSerializer
    queryset = Exterior.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.ExteriorSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class GarageSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.GarageCarportSerializer
    queryset = GarageCarport.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.GarageCarportSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class KitchenSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.KitchenSerializer
    queryset = Kitchen.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.KitchenSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class LaundrySectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.LaundrySerializer
    queryset = Laundry.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.LaundrySerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class InteriorSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.InteriorSerializer
    queryset = Interior.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.InteriorSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class BasementSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.BasementSerializer
    queryset = Basement.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.BasementSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class CrawlspaceSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.CrawlspaceSerializer
    queryset = Crawlspace.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.CrawlspaceSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class PlumbingSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.PlumbingSerializer
    queryset = Plumbing.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.PlumbingSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class BathroomSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.BathroomSerializer
    queryset = Bathroom.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.BathroomSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class HeatingSystemsSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.HeatingSystemSerializer
    queryset = HeatingSystem.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.HeatingSystemSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class ElectricalSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.ElectricalCoolingSystemsSerializer
    queryset = ElectricalCoolingSystems.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.ElectricalCoolingSystemsSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class BedroomSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.BedroomSerializer
    queryset = Bedrooms.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.BedroomSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class LivingRoomSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.LivingRoomSerializer
    queryset = LivingRoom.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.LivingRoomSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


class DiningRoomSectionViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.DiningRoomSerializer
    queryset = DiningRoom.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset

        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.DiningRoomSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new basement record."""
        serializer.save(user=self.request.user)


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                'assigned_only',
                OpenApiTypes.INT, enum=[0, 1],
                description='Filter by items assigned to reports.',
            ),
        ]
    )
)
class PhotoAndCommentsViewSet(viewsets.ModelViewSet):
    """Manage Upload Photos in the database."""
    serializer_class = serializers.PhotoAndCommentsSerializer
    queryset = PhotoAndComments.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]


    def get_queryset(self):
        """Filter queryset to authenticated user."""
        pk = self.kwargs.get('pk')
        return PhotoAndComments.objects.filter(uuid=pk)


    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if not  serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        instance = self.perform_create(serializer)

        response_data = {
            "message": "Details Uploaded Successfully",
            "url": instance.photo.url,
            "comments": instance.comments,
            "uuid": instance.pk
        }
        return Response(response_data, status=status.HTTP_201_CREATED)



    def perform_create(self, serializer):
        return serializer.save()

    def partial_update(self, request, *args, **kwargs):
        """Handle PATCH request with pk."""
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        instance = self.perform_update(serializer)
        response_data = {
            "message": "Details Updated Successfully",
            "url": instance.photo.url,
            "comments": instance.comments,
            "uuid": instance.pk
        }
        return Response(response_data, status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        return serializer.save()



class GroundsDrivewayPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsDrivewayPhotosSerializer
    queryset = GroundsDrivewayPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_grounds_driveway_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'grounds_driveway_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsSidewalkPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsSidewalkPhotosSerializer
    queryset = GroundsSideWalksPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_grounds_sidewalk_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'grounds_sidewalk_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsCoverPhotosView(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsCoversPhotosSerializer
    queryset = GroundsCoversPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_grounds_cover_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'grounds_cover_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsStoopStepsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsStoopStepsPhotosSerializer
    queryset = GroundsStoopStepsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_kitchen_appliances_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'grounds_stoop_steps_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsPorchPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsPorchPhotosSerializer
    queryset = GroundsPorchPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_grounds_porch_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'grounds_porch_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsPatioPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsPatioPhotosSerializer
    queryset = GroundsPatioPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_grounds_patio_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'grounds_patio_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsDeckPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsDeckPhotosSerializer
    queryset = GroundsDeckPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_grounds_deck_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'grounds_deck_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsFencePhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsFencePhotosSerializer
    queryset = GroundsFencePhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_grounds_fence_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'grounds_fence_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsLandscapingPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsLandscapingPhotosSerializer
    queryset = GroundsLandscapingPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_grounds_landscaping_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'grounds_landscaping_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsRetainingWallPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsRetainingWallPhotosSerializer
    queryset = GroundsRetainingWallPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_grounds_retaining_wall_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'grounds_retaining_wall_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsHoseBibsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsHoseBibsPhotosSerializer
    queryset = GroundsHoseBibsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_grounds_hose_bibs_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'grounds_hose_bibs_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorChimneyPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorChimneyPhotosSerializer
    queryset = ExteriorChimneyPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_exterior_chimney_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'exterior_chimney_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorSidingPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorSidingPhotosSerializer
    queryset = ExteriorSidingPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_exterior_siding_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'exterior_siding_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorTrimPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorTrimPhotosSerializer
    queryset = ExteriorTrimPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_exterior_trim_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'exterior_trim_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorGuttersPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorGuttersPhotosSerializer
    queryset = ExteriorGuttersPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_exterior_gutters_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'exterior_gutters_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorSoffitPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorSoffitPhotosSerializer
    queryset = ExteriorSoffitPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_exterior_soffit_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'exterior_soffit_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorFasciaPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorFasciaPhotosSerializer
    queryset = ExteriorFasciaPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_exterior_fascia_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'exterior_fascia_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorFlashingPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorFlashingPhotosSerializer
    queryset = ExteriorFlashingPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_exterior_flashing_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'exterior_flashing_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorSlabOnGradePhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorSlabOnGradePhotosSerializer
    queryset = ExteriorSlabOnGradePhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_exterior_slab_on_grade_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'exterior_slab_on_grade_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorElectricalPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorElectricalPhotosSerializer
    queryset = ExteriorElectricalPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_exterior_service_entry_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'exterior_electrical_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorWindowsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorWindowsPhotosSerializer
    queryset = ExteriorWindowsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_exterior_windows_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'exterior_windows_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorDoorsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorDoorsPhotosSerializer
    queryset = ExteriorDoorsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_exterior_doors_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'exterior_doors_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorCaulkingPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorCaulkingPhotosSerializer
    queryset = ExteriorCaulkingPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_exterior_caulking_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'exterior_caulking_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RoofGeneralPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.RoofGeneralPhotosSerializer
    queryset = RoofGeneralPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_roof_general_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'roof_general_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RoofStylePhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.RoofStylePhotosSerializer
    queryset = RoofStylePhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_roof_style_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'roof_style_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RoofVentilationPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.RoofVentilationPhotosSerializer
    queryset = RoofVentilationPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_roof_ventilation_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'roof_ventilation_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RoofFlashingPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.RoofFlashingPhotosSerializer
    queryset = RoofFlashingPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_roof_flashing_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'roof_flashing_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RoofValleysPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.RoofValleysPhotosSerializer
    queryset = RoofValleysPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_roof_valleys_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'roof_valleys_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RoofConditionPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.RoofConditionPhotosSerializer
    queryset = RoofConditionPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_roof_condition_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'roof_condition_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RoofSkylightsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.RoofSkylightsPhotosSerializer
    queryset = RoofSkylightsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_roof_sskylights_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'roof_skylights_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RoofPlumbingVentsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.RoofPlumbingVentsPhotosSerializer
    queryset = RoofPlumbingVentsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_roof_plumbing_vents_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'roof_plumbing_vents_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageTypePhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageTypePhotosSerializer
    queryset = GarageTypePhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_garage_type_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'garage_type_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageAutomaticOpenerPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageAutomaticOpenerPhotosSerializer
    queryset = GarageAutomaticOpenerPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_garage_automatic_opener_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'garage_automatic_opener_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageSafetyReversePhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageSafetyReversePhotosSerializer
    queryset = GarageSafetyReversePhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_garage_safety_reverse_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'garage_safety_reverse_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageRoofingPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageRoofingPhotosSerializer
    queryset = GarageRoofingPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_garage_roofing_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'garage_roofing_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageGuttersPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageGuttersPhotosSerializer
    queryset = GarageGuttersPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_garage_gutters_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'garage_gutters_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageSidingPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageSidingPhotosSerializer
    queryset = GarageSidingPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_garage_siding_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'garage_siding_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageTrimPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageTrimPhotosSerializer
    queryset = GarageTrimPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_garage_trim_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'garage_trim_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageFloorPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageFloorPhotosSerializer
    queryset = GarageFloorPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_garage_floor_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'garage_floor_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageSillPlatesPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageSillPlatesPhotosSerializer
    queryset = GarageSillPlatesPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_garage_sill_plates_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'garage_sill_plates_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageOverheadDoorPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageOverheadDoorPhotosSerializer
    queryset = GarageOverheadDoorPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_garage_overhead_door_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'garage_overhead_door_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageExteriorDoorPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageExteriorDoorPhotosSerializer
    queryset = GarageExteriorDoorPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_garage_exterior_door_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'garage_exterior_door_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageElectricalPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageElectricalPhotosSerializer
    queryset = GarageElectricalPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_garage_electrical_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'garage_electrical_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageFireSeperationPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageFireSeperationPhotosSerializer
    queryset = GarageFireSeperationPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_garage_fire_seperation_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'garage_fire_seperation_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class KitchenCabinetsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.KitchenCabinetsPhotosSerializer
    queryset = KitchenCabinetsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_kitchen_cabinets_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'kitchen_cabinets_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class KitchenCountertopPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.KitchenCountertopPhotosSerializer
    queryset = KitchenCountertopPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_kitchen_countertops_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'kitchen_countertop_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class KitchenPlumbingPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.KitchenPlumbingPhotosSerializer
    queryset = KitchenPlumbingPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_kitchen_plumbing_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'kitchen_plumbing_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class KitchenFloorPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.KitchenFloorPhotosSerializer
    queryset = KitchenFloorPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_kitchen_floor_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'kitchen_floor_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class KitchenWallsCeilingPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.KitchenWallsCeilingPhotosSerializer
    queryset = KitchenWallsCeilingPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_kitchen_walls_ceiling_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'kitchen_walls_ceiling_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class KitchenAppliancesPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.KitchenAppliancesPhotosSerializer
    queryset = KitchenAppliancesPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_kitchen_appliances_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'kitchen_appliances_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementStairsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementStairsPhotosSerializer
    queryset = BasementStairsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_basement_stairs_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'basement_stairs_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementFoundationPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementFoundationPhotosSerializer
    queryset = BasementFoundationPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_basement_foundation_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'basement_foundation_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementDrainagePhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementDrainagePhotosSerializer
    queryset = BasementDrainagePhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_basement_drainage_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'basement_drainage_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementFloorPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementFloorPhotosSerializer
    queryset = BasementFloorPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_basement_floor_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'basement_floor_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementSeismicBoltsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementSeismicBoltsPhotosSerializer
    queryset = BasementSeismicBoltsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_basement_seismic_bolts_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'basement_seismic_bolts_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementGirdersBeamsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementGirdersBeamsPhotosSerializer
    queryset = BasementGirdersBeamsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_basement_girders_beams_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'basement_girders_beams_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementColumnsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementColumnsPhotosSerializer
    queryset = BasementColumnsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_basement_columns_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'basement_columns_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementJoistsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementJoistsPhotosSerializer
    queryset = BasementJoistsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_basement_joists_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'basement_joists_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementSubfloorPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementSubfloorPhotosSerializer
    queryset = BasementSubfloorPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_basement_subfloor_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'basement_subfloor_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceCrawlPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceCrawlPhotosSerializer
    queryset = CrawlspaceCrawlPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_crawlspace_crawl_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'crawlspace_crawl_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceAccessPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceAccessPhotosSerializer
    queryset = CrawlspaceAccessPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_crawlspace_access_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'crawlspace_access_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceFoundationPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceFoundationPhotosSerializer
    queryset = CrawlspaceFoundationPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_crawlspace_foundation_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'crawlspace_foundation_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceFloorPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceFloorPhotosSerializer
    queryset = CrawlspaceFloorPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_crawlspace_floor_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'crawlspace_floor_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceDrainagePhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceDrainagePhotosSerializer
    queryset = CrawlspaceDrainagePhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_crawlspace_drainage_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'crawlspace_drainage_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceSeismicBoltsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceSeismicBoltsPhotosSerializer
    queryset = CrawlspaceSeismicBoltsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_crawlspace_seismic_bolts_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'crawlspace_seismic_bolts_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceVentilationPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceVentilationPhotosSerializer
    queryset = CrawlspaceVentilationPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_crawlspace_ventilation_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'crawlspace_ventilation_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceGirdersBeamsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceGirdersBeamsPhotosSerializer
    queryset = CrawlspaceGirdersBeamsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_crawlspace_girders_beams_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'crawlspace_girders_beams_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceJoistsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceJoistsPhotosSerializer
    queryset = CrawlspaceJoistsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_crawlspace_joists_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'crawlspace_joist_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceSubfloorPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceSubfloorPhotosSerializer
    queryset = CrawlspaceSubfloorPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_crawlspace_subfloor_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'crawlspace_subfloor_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceInsulationPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceInsulationPhotosSerializer
    queryset = CrawlspaceInsulationPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_crawlspace_insulation_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'crawlspace_insulation_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceVaporBarrierPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceVaporBarrierPhotosSerializer
    queryset = CrawlspaceVaporBarrierPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_crawlspace_vapor_barrier_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'crawlspace_vapor_barrier_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class PlumbingWaterServicePhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.PlumbingWaterServicePhotosSerializer
    queryset = PlumbingWaterServicePhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_plumbing_water_service_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'plumbing_water_service_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class PlumbingGasShutoffPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.PlumbingGasShutoffPhotosSerializer
    queryset = PlumbingGasShutoffPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_plumbing_gas_shutoff_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'plumbing_gas_shutoff_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class PlumbingWaterHeaterPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.PlumbingWaterHeaterPhotosSerializer
    queryset = PlumbingWaterHeaterPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_plumbing_water_heater_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'plumbing_water_heater_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class PlumbingWellPumpPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.PlumbingWellPumpPhotosSerializer
    queryset = PlumbingWellPumpPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_plumbing_well_pump_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'plumbing_well_pump_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class PlumbingSanitaryPumpPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.PlumbingSanitaryPumpPhotosSerializer
    queryset = PlumbingSanitaryPumpPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_plumbing_sanitary_pump_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'plumbing_sanitary_pump_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BathroomElectricalPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BathroomElectricalPhotosSerializer
    queryset = BathroomElectricalPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_bathroom_electrical_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'bathroom_electrical_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BathroomPlumbingPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BathroomPlumbingPhotosSerializer
    queryset = BathroomPlumbingPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_bathroom_plumbing_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'bathroom_plumbing_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class HeatingFurnacePhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.FurnacePhotosSerializer
    queryset = FurnacePhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_heating_furnace_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'heating_furnace_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class HeatingBoilerPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BoilerPhotosSerializer
    queryset = BoilerPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_heating_boiler_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'heating_boiler_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class HeatingOtherSystemsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.HeatingOtherSystemsPhotosSerializer
    queryset = HeatingOtherSystemsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_heating_other_systems_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'heating_other_systems_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorCoolingUnitPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorCoolingUnitPhotosSerializer
    queryset = ExteriorCoolingUnitPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_exterior_cooling_unit_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'exterior_cooling_unit_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class HeatingEvaporatorCoilPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.EvaporatorCoilPhotosSerializer
    queryset = EvaporatorCoilPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_heating_evaporator_coil_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'heating_evaporator_coil_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ElectricalMainPanelPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ElectricalMainPanelPhotosSerializer
    queryset = ElectricalMainPanelPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_electrical_main_panel_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'electrical_main_panel_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ElectricalSubPanelPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ElectricalSubPanelPhotosSerializer
    queryset = ElectricalSubPanelPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_electrical_sub_panel_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'electrical_sub_panel_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class LaundryPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.LaundryPhotosSerializer
    queryset = LaundryPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_laundry_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'laundry_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class InteriorFireplacePhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.InteriorFireplacePhotosSerializer
    queryset = InteriorFireplacePhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_interior_fireplace_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'interior_fireplace_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class InteriorStepsPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.InteriorStepsPhotosSerializer
    queryset = InteriorStepsPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_interior_steps_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'interior_steps_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class InteriorSmokeCarbonDetPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.InteriorSmokeCarbonDetPhotosSerializer
    queryset = InteriorSmokeCarbonDetPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_interior_smoke_carbon_det_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'interior_smoke_carbon_det_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class InteriorAtticPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.InteriorAtticPhotosSerializer
    queryset = InteriorAtticPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_interior_attic_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'interior_attic_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BedroomPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BedroomPhotosSerializer
    queryset = BedroomPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_bedroom_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'bedroom_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class LivingRoomPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.LivingRoomPhotosSerializer
    queryset = LivingRoomPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_living_room_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'living_room_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class DiningRoomPhotosViewSet(viewsets.ModelViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.DiningRoomPhotosSerializer
    queryset = DiningRoomPhotos.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_dining_room_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'dining_room_photos')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)




class UserCommentsSummaryViewSet(mixins.DestroyModelMixin,
                                    mixins.UpdateModelMixin,
                                    mixins.ListModelMixin,
                                    mixins.CreateModelMixin,
                                    viewsets.GenericViewSet):
    """Manage All the comments in the database."""
    serializer_class = serializers.UserCommentsSummarySerializer
    queryset = UserCommentsSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)





class GroundsDrivewaySummaryViewSet(mixins.DestroyModelMixin,
                                    mixins.UpdateModelMixin,
                                    mixins.ListModelMixin,
                                    mixins.CreateModelMixin,
                                    viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsDrivewaySummarySerializer
    queryset = GroundsDrivewaySummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsSidewalkSummaryViewSet(mixins.DestroyModelMixin,
                                    mixins.UpdateModelMixin,
                                    mixins.ListModelMixin,
                                    mixins.CreateModelMixin,
                                    viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsSidewalkSummarySerializer
    queryset = GroundsSidewalkSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsStoopStepsSummaryViewSet(mixins.DestroyModelMixin,
                                      mixins.UpdateModelMixin,
                                      mixins.ListModelMixin,
                                      mixins.CreateModelMixin,
                                      viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsStoopStepsSummarySerializer
    queryset = GroundsStoopStepsSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsPorchSummaryViewSet(mixins.DestroyModelMixin,
                                 mixins.UpdateModelMixin,
                                 mixins.ListModelMixin,
                                 mixins.CreateModelMixin,
                                 viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsPorchSummarySerializer
    queryset = GroundsPorchSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsCoversSummaryViewSet(mixins.DestroyModelMixin,
                                  mixins.UpdateModelMixin,
                                  mixins.ListModelMixin,
                                  mixins.CreateModelMixin,
                                  viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsCoversSummarySerializer
    queryset = GroundsCoversSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsPatioSummaryViewSet(mixins.DestroyModelMixin,
                                 mixins.UpdateModelMixin,
                                 mixins.ListModelMixin,
                                 mixins.CreateModelMixin,
                                 viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsPatioSummarySerializer
    queryset = GroundsPatioSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsDeckSummaryViewSet(mixins.DestroyModelMixin,
                                mixins.UpdateModelMixin,
                                mixins.ListModelMixin,
                                mixins.CreateModelMixin,
                                viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsDeckSummarySerializer
    queryset = GroundsDeckSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsFenceSummaryViewSet(mixins.DestroyModelMixin,
                                 mixins.UpdateModelMixin,
                                 mixins.ListModelMixin,
                                 mixins.CreateModelMixin,
                                 viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsFenceSummarySerializer
    queryset = GroundsFenceSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsLandscapingSummaryViewSet(mixins.DestroyModelMixin,
                                       mixins.UpdateModelMixin,
                                       mixins.ListModelMixin,
                                       mixins.CreateModelMixin,
                                       viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsLandscapingSummarySerializer
    queryset = GroundsLandscapingSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsRetainingWallSummaryViewSet(mixins.DestroyModelMixin,
                                         mixins.UpdateModelMixin,
                                         mixins.ListModelMixin,
                                         mixins.CreateModelMixin,
                                         viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsRetainingWallSummarySerializer
    queryset = GroundsRetainingWallSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroundsHoseBibsSummaryViewSet(mixins.DestroyModelMixin,
                                    mixins.UpdateModelMixin,
                                    mixins.ListModelMixin,
                                    mixins.CreateModelMixin,
                                    viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GroundsHoseBibsSummarySerializer
    queryset = GroundsHoseBibsSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RoofGeneralSummaryViewSet(mixins.DestroyModelMixin,
                                mixins.UpdateModelMixin,
                                mixins.ListModelMixin,
                                mixins.CreateModelMixin,
                                viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.RoofGeneralSummarySerializer
    queryset = RoofGeneralSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RoofStyleSummaryViewSet(mixins.DestroyModelMixin,
                              mixins.UpdateModelMixin,
                              mixins.ListModelMixin,
                              mixins.CreateModelMixin,
                              viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.RoofStyleSummarySerializer
    queryset = RoofStyleSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RoofVentilationSummaryViewSet(mixins.DestroyModelMixin,
                                    mixins.UpdateModelMixin,
                                    mixins.ListModelMixin,
                                    mixins.CreateModelMixin,
                                    viewsets.GenericViewSet,):
    """Manage comments in the database."""
    serializer_class = serializers.RoofVentilationSummarySerializer
    queryset = RoofVentilationSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RoofFlashingSummaryViewSet(mixins.DestroyModelMixin,
                                 mixins.UpdateModelMixin,
                                 mixins.ListModelMixin,
                                 mixins.CreateModelMixin,
                                 viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.RoofFlashingSummarySerializer
    queryset = RoofFlashingSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RoofValleysSummaryViewSet(mixins.DestroyModelMixin,
                                mixins.UpdateModelMixin,
                                mixins.ListModelMixin,
                                mixins.CreateModelMixin,
                                viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.RoofValleysSummarySerializer
    queryset = RoofValleysSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RoofConditionSummaryViewSet(mixins.DestroyModelMixin,
                                  mixins.UpdateModelMixin,
                                  mixins.ListModelMixin,
                                  mixins.CreateModelMixin,
                                  viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.RoofConditionSummarySerializer
    queryset = RoofConditionSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RoofSkylightsSummaryViewSet(mixins.DestroyModelMixin,
                                  mixins.UpdateModelMixin,
                                  mixins.ListModelMixin,
                                  mixins.CreateModelMixin,
                                  viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.RoofSkylightsSummarySerializer
    queryset = RoofSkylightsSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RoofPlumbingVentsSummaryViewSet(mixins.DestroyModelMixin,
                                      mixins.UpdateModelMixin,
                                      mixins.ListModelMixin,
                                      mixins.CreateModelMixin,
                                      viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.RoofPlumbingVentsSummarySerializer
    queryset = RoofPlumbingVentsSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorChimneySummaryViewSet(mixins.DestroyModelMixin,
                                    mixins.UpdateModelMixin,
                                    mixins.ListModelMixin,
                                    mixins.CreateModelMixin,
                                    viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorChimneySummarySerializer
    queryset = ExteriorChimneySummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorGuttersSummaryViewSet(mixins.DestroyModelMixin,
                                    mixins.UpdateModelMixin,
                                    mixins.ListModelMixin,
                                    mixins.CreateModelMixin,
                                    viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorGuttersSummarySerializer
    queryset = ExteriorGuttersSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorSidingSummaryViewSet(mixins.DestroyModelMixin,
                                   mixins.UpdateModelMixin,
                                   mixins.ListModelMixin,
                                   mixins.CreateModelMixin,
                                   viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorSidingSummarySerializer
    queryset = ExteriorSidingSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorTrimSummaryViewSet(mixins.DestroyModelMixin,
                                 mixins.UpdateModelMixin,
                                 mixins.ListModelMixin,
                                 mixins.CreateModelMixin,
                                 viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorTrimSummarySerializer
    queryset = ExteriorTrimSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorCaulkingSummaryViewSet(mixins.DestroyModelMixin,
                                     mixins.UpdateModelMixin,
                                     mixins.ListModelMixin,
                                     mixins.CreateModelMixin,
                                     viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorCaulkingSummarySerializer
    queryset = ExteriorCaulkingSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorFlashingSummaryViewSet(mixins.DestroyModelMixin,
                                     mixins.UpdateModelMixin,
                                     mixins.ListModelMixin,
                                     mixins.CreateModelMixin,
                                     viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorFlashingSummarySerializer
    queryset = ExteriorFlashingSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorFasciaSummaryViewSet(mixins.DestroyModelMixin,
                                   mixins.UpdateModelMixin,
                                   mixins.ListModelMixin,
                                   mixins.CreateModelMixin,
                                   viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorFasciaSummarySerializer
    queryset = ExteriorFasciaSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorSoffitSummaryViewSet(mixins.DestroyModelMixin,
                                   mixins.UpdateModelMixin,
                                   mixins.ListModelMixin,
                                   mixins.CreateModelMixin,
                                   viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorSoffitSummarySerializer
    queryset = ExteriorSoffitSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorSlabOnFoundationSummaryViewSet(mixins.DestroyModelMixin,
                                             mixins.UpdateModelMixin,
                                             mixins.ListModelMixin,
                                             mixins.CreateModelMixin,
                                             viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorSlabOnFoundationSummarySerializer
    queryset = ExteriorSlabOnFoundationSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorServiceEntrySummaryViewSet(mixins.DestroyModelMixin,
                                         mixins.UpdateModelMixin,
                                         mixins.ListModelMixin,
                                         mixins.CreateModelMixin,
                                         viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorServiceEntrySummarySerializer
    queryset = ExteriorServiceEntrySummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorWallConstructionSummaryViewSet(mixins.DestroyModelMixin,
                                             mixins.UpdateModelMixin,
                                             mixins.ListModelMixin,
                                             mixins.CreateModelMixin,
                                             viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorWallConstructionSummarySerializer
    queryset = ExteriorWallConstructionSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorWindowsSummaryViewSet(mixins.DestroyModelMixin,
                                    mixins.UpdateModelMixin,
                                    mixins.ListModelMixin,
                                    mixins.CreateModelMixin,
                                    viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorWindowsSummarySerializer
    queryset = ExteriorWindowsSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ExteriorDoorsSummaryViewSet(mixins.DestroyModelMixin,
                                  mixins.UpdateModelMixin,
                                  mixins.ListModelMixin,
                                  mixins.CreateModelMixin,
                                  viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ExteriorDoorsSummarySerializer
    queryset = ExteriorDoorsSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageTypeSummaryViewSet(mixins.DestroyModelMixin,
                               mixins.UpdateModelMixin,
                               mixins.ListModelMixin,
                               mixins.CreateModelMixin,
                               viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageTypeSummarySerializer
    queryset = GarageTypeSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageAutomaticOpenerSummaryViewSet(mixins.DestroyModelMixin,
                                          mixins.UpdateModelMixin,
                                          mixins.ListModelMixin,
                                          mixins.CreateModelMixin,
                                          viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageAutomaticOpenerSummarySerializer
    queryset = GarageAutomaticOpenerSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageSafetyReverseSummaryViewSet(mixins.DestroyModelMixin,
                                        mixins.UpdateModelMixin,
                                        mixins.ListModelMixin,
                                        mixins.CreateModelMixin,
                                        viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageSafetyReverseSummarySerializer
    queryset = GarageSafetyReverseSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageGuttersSummaryViewSet(mixins.DestroyModelMixin,
                                  mixins.UpdateModelMixin,
                                  mixins.ListModelMixin,
                                  mixins.CreateModelMixin,
                                  viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageGuttersSummarySerializer
    queryset = GarageGuttersSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageSidingSummaryViewSet(mixins.DestroyModelMixin,
                                 mixins.UpdateModelMixin,
                                 mixins.ListModelMixin,
                                 mixins.CreateModelMixin,
                                 viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageSidingSummarySerializer
    queryset = GarageSidingSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageFloorSummaryViewSet(mixins.DestroyModelMixin,
                                mixins.UpdateModelMixin,
                                mixins.ListModelMixin,
                                mixins.CreateModelMixin,
                                viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageFloorSummarySerializer
    queryset = GarageFloorSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageSillPlatesSummaryViewSet(mixins.DestroyModelMixin,
                                     mixins.UpdateModelMixin,
                                     mixins.ListModelMixin,
                                     mixins.CreateModelMixin,
                                     viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageSillPlatesSummarySerializer
    queryset = GarageSillPlatesSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageElectricalSummaryViewSet(mixins.DestroyModelMixin,
                                     mixins.UpdateModelMixin,
                                     mixins.ListModelMixin,
                                     mixins.CreateModelMixin,
                                     viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageElectricalSummarySerializer
    queryset = GarageElectricalSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageOverheadDoorSummaryViewSet(mixins.DestroyModelMixin,
                                       mixins.UpdateModelMixin,
                                       mixins.ListModelMixin,
                                       mixins.CreateModelMixin,
                                       viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageOverheadDoorSummarySerializer
    queryset = GarageOverheadDoorSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageExteriorDoorSummaryViewSet(mixins.DestroyModelMixin,
                                       mixins.UpdateModelMixin,
                                       mixins.ListModelMixin,
                                       mixins.CreateModelMixin,
                                       viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageExteriorDoorSummarySerializer
    queryset = GarageExteriorDoorSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GarageFireSeperationSummaryViewSet(mixins.DestroyModelMixin,
                                         mixins.UpdateModelMixin,
                                         mixins.ListModelMixin,
                                         mixins.CreateModelMixin,
                                         viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.GarageFireSeperationSummarySerializer
    queryset = GarageFireSeperationSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementStairsSummaryViewSet(mixins.DestroyModelMixin,
                                   mixins.UpdateModelMixin,
                                   mixins.ListModelMixin,
                                   mixins.CreateModelMixin,
                                   viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementStairsSummarySerializer
    queryset = BasementStairsSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementFoundationSummaryViewSet(mixins.DestroyModelMixin,
                                       mixins.UpdateModelMixin,
                                       mixins.ListModelMixin,
                                       mixins.CreateModelMixin,
                                       viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementFoundationSummarySerializer
    queryset = BasementFoundationSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementFloorSummaryViewSet(mixins.DestroyModelMixin,
                                  mixins.UpdateModelMixin,
                                  mixins.ListModelMixin,
                                  mixins.CreateModelMixin,
                                  viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementFloorSummarySerializer
    queryset = BasementFloorSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementDrainageSummaryViewSet(mixins.DestroyModelMixin,
                                     mixins.UpdateModelMixin,
                                     mixins.ListModelMixin,
                                     mixins.CreateModelMixin,
                                     viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementDrainageSummarySerializer
    queryset = BasementDrainageSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementSeismicBoltsSummaryViewSet(mixins.DestroyModelMixin,
                                         mixins.UpdateModelMixin,
                                         mixins.ListModelMixin,
                                         mixins.CreateModelMixin,
                                         viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementSeismicBoltsSummarySerializer
    queryset = BasementSeismicBoltsSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementGirdersBeamsSummaryViewSet(mixins.DestroyModelMixin,
                                         mixins.UpdateModelMixin,
                                         mixins.ListModelMixin,
                                         mixins.CreateModelMixin,
                                         viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementGirdersBeamsSummarySerializer
    queryset = BasementGirdersBeamsSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementColumnsSummaryViewSet(mixins.DestroyModelMixin,
                                    mixins.UpdateModelMixin,
                                    mixins.ListModelMixin,
                                    mixins.CreateModelMixin,
                                    viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementColumnsSummarySerializer
    queryset = BasementColumnsSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementJoistsSummaryViewSet(mixins.DestroyModelMixin,
                                   mixins.UpdateModelMixin,
                                   mixins.ListModelMixin,
                                   mixins.CreateModelMixin,
                                   viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementJoistsSummarySerializer
    queryset = BasementJoistsSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BasementSubfloorSummaryViewSet(mixins.DestroyModelMixin,
                                     mixins.UpdateModelMixin,
                                     mixins.ListModelMixin,
                                     mixins.CreateModelMixin,
                                     viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BasementSubfloorSummarySerializer
    queryset = BasementSubfloorSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceCrawlSummaryViewSet(mixins.DestroyModelMixin,
                                    mixins.UpdateModelMixin,
                                    mixins.ListModelMixin,
                                    mixins.CreateModelMixin,
                                    viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceCrawlSummarySerializer
    queryset = CrawlspaceCrawlSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceAccessSummaryViewSet(mixins.DestroyModelMixin,
                                     mixins.UpdateModelMixin,
                                     mixins.ListModelMixin,
                                     mixins.CreateModelMixin,
                                     viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceAccessSummarySerializer
    queryset = CrawlspaceAccessSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceVentilationSummaryViewSet(mixins.DestroyModelMixin,
                                          mixins.UpdateModelMixin,
                                          mixins.ListModelMixin,
                                          mixins.CreateModelMixin,
                                          viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceVentilationSummarySerializer
    queryset = CrawlspaceVentilationSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceInsulationSummaryViewSet(mixins.DestroyModelMixin,
                                         mixins.UpdateModelMixin,
                                         mixins.ListModelMixin,
                                         mixins.CreateModelMixin,
                                         viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceInsulationSummarySerializer
    queryset = CrawlspaceInsulationSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CrawlspaceVaporBarrierSummaryViewSet(mixins.DestroyModelMixin,
                                           mixins.UpdateModelMixin,
                                           mixins.ListModelMixin,
                                           mixins.CreateModelMixin,
                                           viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.CrawlspaceVaporBarrierSummarySerializer
    queryset = CrawlspaceVaporBarrierSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class KitchenCountertopSummaryViewSet(mixins.DestroyModelMixin,
                                      mixins.UpdateModelMixin,
                                      mixins.ListModelMixin,
                                      mixins.CreateModelMixin,
                                      viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.KitchenCountertopSummarySerializer
    queryset = KitchenCountertopSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class KitchenCabinetsSummaryViewSet(mixins.DestroyModelMixin,
                                    mixins.UpdateModelMixin,
                                    mixins.ListModelMixin,
                                    mixins.CreateModelMixin,
                                    viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.KitchenCabinetsSummarySerializer
    queryset = KitchenCabinetsSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class KitchenPlumbingSummaryViewSet(mixins.DestroyModelMixin,
                                    mixins.UpdateModelMixin,
                                    mixins.ListModelMixin,
                                    mixins.CreateModelMixin,
                                    viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.KitchenPlumbingSummarySerializer
    queryset = KitchenPlumbingSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class KitchenFloorSummaryViewSet(mixins.DestroyModelMixin,
                                 mixins.UpdateModelMixin,
                                 mixins.ListModelMixin,
                                 mixins.CreateModelMixin,
                                 viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.KitchenFloorSummarySerializer
    queryset = KitchenFloorSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class KitchenWallsCeilingSummaryViewSet(mixins.DestroyModelMixin,
                                        mixins.UpdateModelMixin,
                                        mixins.ListModelMixin,
                                        mixins.CreateModelMixin,
                                        viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.KitchenWallsCeilingSummarySerializer
    queryset = KitchenWallsCeilingSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class KitchenAppliancesSummaryViewSet(mixins.DestroyModelMixin,
                                      mixins.UpdateModelMixin,
                                      mixins.ListModelMixin,
                                      mixins.CreateModelMixin,
                                      viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.KitchenAppliancesSummarySerializer
    queryset = KitchenAppliancesSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BathroomElectricalSummaryViewSet(mixins.DestroyModelMixin,
                                       mixins.UpdateModelMixin,
                                       mixins.ListModelMixin,
                                       mixins.CreateModelMixin,
                                       viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BathroomElectricalSummarySerializer
    queryset = BathroomElectricalSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BathroomPlumbingSummaryViewSet(mixins.DestroyModelMixin,
                                     mixins.UpdateModelMixin,
                                     mixins.ListModelMixin,
                                     mixins.CreateModelMixin,
                                     viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BathroomPlumbingSummarySerializer
    queryset = BathroomPlumbingSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class PlumbingWaterServiceSummaryViewSet(mixins.DestroyModelMixin,
                                         mixins.UpdateModelMixin,
                                         mixins.ListModelMixin,
                                         mixins.CreateModelMixin,
                                         viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.PlumbingWaterServiceSummarySerializer
    queryset = PlumbingWaterServiceSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class PlumbingGasShutoffSummaryViewSet(mixins.DestroyModelMixin,
                                       mixins.UpdateModelMixin,
                                       mixins.ListModelMixin,
                                       mixins.CreateModelMixin,
                                       viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.PlumbingGasShutoffSummarySerializer
    queryset = PlumbingGasShutoffSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class PlumbingWaterHeaterSummaryViewSet(mixins.DestroyModelMixin,
                                        mixins.UpdateModelMixin,
                                        mixins.ListModelMixin,
                                        mixins.CreateModelMixin,
                                        viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.PlumbingWaterHeaterSummarySerializer
    queryset = PlumbingWaterHeaterSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ElectricalMainPanelSummaryViewSet(mixins.DestroyModelMixin,
                                        mixins.UpdateModelMixin,
                                        mixins.ListModelMixin,
                                        mixins.CreateModelMixin,
                                        viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ElectricalMainPanelSummarySerializer
    queryset = ElectricalMainPanelSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ElectricalSubPanelSummaryViewSet(mixins.DestroyModelMixin,
                                       mixins.UpdateModelMixin,
                                       mixins.ListModelMixin,
                                       mixins.CreateModelMixin,
                                       viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.ElectricalSubPanelSummarySerializer
    queryset = ElectricalSubPanelSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class InteriorFireplaceSummaryViewSet(mixins.DestroyModelMixin,
                                      mixins.UpdateModelMixin,
                                      mixins.ListModelMixin,
                                      mixins.CreateModelMixin,
                                      viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.InteriorFireplaceSummarySerializer
    queryset = InteriorFireplaceSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class InteriorStepsSummaryViewSet(mixins.DestroyModelMixin,
                                  mixins.UpdateModelMixin,
                                  mixins.ListModelMixin,
                                  mixins.CreateModelMixin,
                                  viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.InteriorStepsSummarySerializer
    queryset = InteriorStepsSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class InteriorSmokeCarbonDetSummaryViewSet(mixins.DestroyModelMixin,
                                           mixins.UpdateModelMixin,
                                           mixins.ListModelMixin,
                                           mixins.CreateModelMixin,
                                           viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.InteriorSmokeCarbonDetSummarySerializer
    queryset = InteriorSmokeCarbonDetSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class InteriorAtticSummaryViewSet(mixins.DestroyModelMixin,
                                  mixins.UpdateModelMixin,
                                  mixins.ListModelMixin,
                                  mixins.CreateModelMixin,
                                  viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.InteriorAtticSummarySerializer
    queryset = InteriorAtticSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BedroomSummaryViewSet(mixins.DestroyModelMixin,
                            mixins.UpdateModelMixin,
                            mixins.ListModelMixin,
                            mixins.CreateModelMixin,
                            viewsets.GenericViewSet):
    """Manage comments in the database."""
    serializer_class = serializers.BedroomSummarySerializer
    queryset = BedroomSummary.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by('-title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


# generate reports funcion
class GenerateReportHTML(APIView):
    """
    Generate reports (GET request, unauthenticated).
    """
    authentication_classes = []  # No authentication classes
    permission_classes = []  # No permission classes


    def get(self, request, *args, **kwargs):
        if 'report_uuid' not in request.GET:
            return JsonResponse({'error': 'provide a report uuid first'}, safe=False)

        # get reports from given report uuid
        report_uuid = request.GET['report_uuid']
        reports = get_reports(report_uuid)
        summary = get_report_summary(report_uuid)


        return render(request, 'report.html', {'reports': reports, 'summary': summary})



class GenerateReportViewSet(viewsets.ModelViewSet):
    """Manage report generation in the database."""
    serializer_class = serializers.GenerateReportSerializer
    queryset = GenerateReport.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]


    def get_queryset(self):
        """Filter queryset to authenticated user."""

        return


    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        # Convert HTML to PDF
        html = HTML(url=f'http://api.staging.ainspectprorestapinafkjsdfl.link/api/report/generate-report-html?report_uuid={serializer.data["report_uuid"]}')
        pdf = html.write_pdf()

        # Return the PDF file as a response
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = f'attachment; filename="report_{serializer.data["report_uuid"]}.pdf"'
        return response



    def perform_create(self, serializer):
        return serializer.save()
