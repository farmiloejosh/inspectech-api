#!/bin/bash

sudo yum update -y
sudo yum -y install docker
sudo yum -y install docker
sudo service docker start
sudo systemctl enable docker
sudo usermod -aG docker ec2-user
