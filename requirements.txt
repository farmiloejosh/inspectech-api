Django>=4.2.5,<4.3
djangorestframework>=3.14,<3.15
psycopg2>=2.8.6,<2.9
drf-spectacular>=0.26.4,<0.26.5
Pillow==10.3.0
uwsgi>=2.0.23, <2.1.0
boto3>=1.12.0,<1.13.0
django-storages>=1.13.0,<1.14.0
weasyprint==62.2
