"""
Database models.
"""
import uuid
import os
from django.conf import settings
from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)
from django.contrib.postgres.fields import ArrayField, JSONField


def get_image_path(instance, filename):
    """Generate file path for An image"""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'images', filename)


def logo_image_file_path(instance, filename):
    """Generate file path for new recipe image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'logo', filename)


def signature_image_file_path(instance, filename):
    """Generate file path for new recipe image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'signature', filename)


def report_details_image_file_path(instance, filename):
    """Generate file path for new recipe image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'report-details', filename)


def grounds_driveway_image_file_path(instance, filename):
    """Generate file path for new grounds driveway image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'grounds-driveway', filename)


def grounds_sidewalk_image_file_path(instance, filename):
    """Generate file path for new grounds sidewalk image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'grounds-sidewalk', filename)


def grounds_stoop_steps_image_file_path(instance, filename):
    """Generate file path for new grounds stoop/steps image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'grounds-stoop-steps', filename)


def grounds_patio_image_file_path(instance, filename):
    """Generate file path for new grounds patio image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'grounds-patio', filename)


def grounds_porch_image_file_path(instance, filename):
    """Generate file path for new grounds porch image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'grounds-porch', filename)


def grounds_deck_image_file_path(instance, filename):
    """Generate file path for new grounds deck image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'grounds-deck', filename)


def grounds_balcony_image_file_path(instance, filename):
    """Generate file path for new grounds balcony image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'grounds-balcony', filename)


def grounds_covers_image_file_path(instance, filename):
    """Generate file path for new grounds patio/porch/balcony covers image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'grounds-covers', filename)


def grounds_fence_image_file_path(instance, filename):
    """Generate file path for new grounds fence image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'grounds-fence', filename)


def grounds_landscaping_image_file_path(instance, filename):
    """Generate file path for new grounds image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'grounds-landscaping', filename)


def grounds_retaining_wall_image_file_path(instance, filename):
    """Generate file path for new grounds retaining wall image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'grounds-retaining-wall', filename)


def grounds_hose_bibs_image_file_path(instance, filename):
    """Generate file path for new grounds hose bib image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'grounds-hose-bib', filename)


def roof_general_image_file_path(instance, filename):
    """Generate file path for new roof general image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'roof-general', filename)


def roof_style_image_file_path(instance, filename):
    """Generate file path for new roof style image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'roof-style', filename)


def roof_ventilation_image_file_path(instance, filename):
    """Generate file path for new roof ventilation image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'roof-ventilation', filename)


def roof_flashing_image_file_path(instance, filename):
    """Generate file path for new roof flashing image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'roof-flashing', filename)


def roof_valleys_image_file_path(instance, filename):
    """Generate file path for new roof valleys image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'roof-valleys', filename)


def roof_condition_image_file_path(instance, filename):
    """Generate file path for new roof condition image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'roof-condition', filename)


def roof_skylights_image_file_path(instance, filename):
    """Generate file path for new roof skylights image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'roof-skylights', filename)


def roof_plumbing_vents_image_file_path(instance, filename):
    """Generate file path for new roof plumbing vents image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'roof-plumbing-vents', filename)


def exterior_chimney_image_file_path(instance, filename):
    """Generate file path for new exterior chimney image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'exterior-chimney', filename)


def exterior_gutters_image_file_path(instance, filename):
    """Generate file path for new exterior gutters image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'exterior-gutters', filename)


def exterior_siding_image_file_path(instance, filename):
    """Generate file path for new garage siding image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'exterior-siding', filename)


def exterior_trim_image_file_path(instance, filename):
    """Generate file path for new exterior trim image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'exterior-trim', filename)


def exterior_soffit_image_file_path(instance, filename):
    """Generate file path for new exterior image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'exterior-soffit', filename)


def exterior_flashing_image_file_path(instance, filename):
    """Generate file path for new exterior flashing image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'exterior-flashing', filename)


def exterior_fascia_image_file_path(instance, filename):
    """Generate file path for new exterior fascia image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'exterior-fascia', filename)


def exterior_caulking_image_file_path(instance, filename):
    """Generate file path for new exterior caulking image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'exterior-caulking', filename)


def exterior_windows_image_file_path(instance, filename):
    """Generate file path for new exterior windows image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'exterior-windows', filename)


def exterior_electrical_image_file_path(instance, filename):
    """Generate file path for new exterior electrical image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'exterior-electrical', filename)


def exterior_slab_on_grade_image_file_path(instance, filename):
    """Generate file path for new exterior electrical image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'exterior-slab-on-grade', filename)


def exterior_doors_image_file_path(instance, filename):
    """Generate file path for new exterior doors image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'exterior-doors', filename)


def garage_type_image_file_path(instance, filename):
    """Generate file path for new garage type image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'garage-type', filename)


def garage_automatic_opener_image_file_path(instance, filename):
    """Generate file path for new garage automatic openerimage."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'garage-automatic-opener', filename)


def garage_roofing_image_file_path(instance, filename):
    """Generate file path for new garage roofing image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'garage-roofing', filename)


def garage_safety_reverse_image_file_path(instance, filename):
    """Generate file path for new garage safety reverse image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'garage-safety-reverse', filename)


def garage_gutters_image_file_path(instance, filename):
    """Generate file path for new garage gutters image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'garage-gutters', filename)


def garage_siding_image_file_path(instance, filename):
    """Generate file path for new garage siding image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'garage-siding', filename)


def garage_trim_image_file_path(instance, filename):
    """Generate file path for new garage trim image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'garage-trim', filename)


def garage_flooring_image_file_path(instance, filename):
    """Generate file path for new garage flooring image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'garage-flooring', filename)


def garage_sill_plates_image_file_path(instance, filename):
    """Generate file path for new garage sill plates image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'garage-sill-plates', filename)


def garage_overhead_doors_image_file_path(instance, filename):
    """Generate file path for new garage overhead doors image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'garage-overhead-doors', filename)


def garage_exterior_door_image_file_path(instance, filename):
    """Generate file path for new garage exterior service door image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'garage-exterior-door', filename)


def garage_electrical_image_file_path(instance, filename):
    """Generate file path for new garage electrical image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'garage-electrical', filename)


def garage_fire_seperation_image_file_path(instance, filename):
    """Generate file path for new garage fire seperation walls
    & ceiling image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'garage-fire-seperation', filename)


def kitchen_countertops_image_file_path(instance, filename):
    """Generate file path for new kitchen countertops image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'kitchen-countertops', filename)


def kitchen_cabinets_image_file_path(instance, filename):
    """Generate file path for new kitchen cabinets image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'kitchen-cabinets', filename)


def kitchen_plumbing_image_file_path(instance, filename):
    """Generate file path for new kitchen plumbing image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'kitchen-plumbing', filename)


def kitchen_walls_ceiling_image_file_path(instance, filename):
    """Generate file path for new kitchen walls & ceiling image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'kitchen-walls-ceiling', filename)


def kitchen_floor_image_file_path(instance, filename):
    """Generate file path for new kitchen floor image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'kitchen-floor', filename)


def kitchen_appliances_image_file_path(instance, filename):
    """Generate file path for new kitchen appliances image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'kitchen-appliances', filename)


def laundry_image_file_path(instance, filename):
    """Generate file path for new laundry image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'laundry', filename)


def bathroom_plumbing_image_file_path(instance, filename):
    """Generate file path for new bathroom plumbing image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'bathroom-plumbing', filename)


def bathroom_electrical_image_file_path(instance, filename):
    """Generate file path for new bathroom electrical image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'bathroom-electrical', filename)


def bedroom_image_file_path(instance, filename):
    """Generate file path for new bedroom image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'bedroom', filename)


def interior_fireplace_image_file_path(instance, filename):
    """Generate file path for new interior image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'interior-fireplace', filename)


def interior_steps_image_file_path(instance, filename):
    """Generate file path for new interior image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'interior-steps', filename)


def interior_smoke_carbon_det_image_file_path(instance, filename):
    """Generate file path for new interior image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'interior-smoke-carbon-det', filename)


def interior_attic_image_file_path(instance, filename):
    """Generate file path for new interior image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'interior-attic', filename)


def plumbing_water_service_image_file_path(instance, filename):
    """Generate file path for new plumbing water service image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'plumbing-water-service', filename)


def plumbing_gas_shutoff_image_file_path(instance, filename):
    """Generate file path for new plumbing gas shutoff image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'plumbing-gas-shutoff', filename)


def plumbing_water_heater_image_file_path(instance, filename):
    """Generate file path for new plumbing water heaterimage."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'plumbing-water-heater', filename)


def plumbing_well_pump_image_file_path(instance, filename):
    """Generate file path for new plumbing water service image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'plumbing-well-pump', filename)


def plumbing_sanitary_pump_image_file_path(instance, filename):
    """Generate file path for new plumbing water service image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'plumbing-sanitary-pump', filename)


def main_panel_image_file_path(instance, filename):
    """Generate file path for new main panel image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'main-panel', filename)


def sub_panel_image_file_path(instance, filename):
    """Generate file path for new sub panel image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'electrical', filename)


def furnace_image_file_path(instance, filename):
    """Generate file path for new furnace image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'furnace', filename)


def boiler_image_file_path(instance, filename):
    """Generate file path for new boiler image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'boiler', filename)


def heating_other_systems_image_file_path(instance, filename):
    """Generate file path for new heating other systems image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'heating-other-systems', filename)


def exterior_cooling_unit_image_file_path(instance, filename):
    """Generate file path for new cooling unit image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'exterior-cooling-unit', filename)


def evaporator_coil_file_path(instance, filename):
    """Generate file path for new crawl image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'evaporator-coil', filename)


def living_room_image_file_path(instance, filename):
    """Generate file path for new living room image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'living_room', filename)


def dining_room_image_file_path(instance, filename):
    """Generate file path for new dining room image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'dining_room', filename)


def basement_stairs_image_file_path(instance, filename):
    """Generate file path for new basement image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'basement-stairs', filename)


def basement_foundation_image_file_path(instance, filename):
    """Generate file path for new basement foundation image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'basement-foundation', filename)


def basement_floor_image_file_path(instance, filename):
    """Generate file path for new basement floor image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'basement-floor', filename)


def basement_seismic_bolts_image_file_path(instance, filename):
    """Generate file path for new basement seismic bolts image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'basement-seismic-bolts', filename)


def basement_drainage_image_file_path(instance, filename):
    """Generate file path for new basement drainage image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'basement-drainage', filename)


def basement_girders_beams_image_file_path(instance, filename):
    """Generate file path for new basement girders/beams image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'basement-girders-beams', filename)


def basement_columns_image_file_path(instance, filename):
    """Generate file path for new basement columns image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'basement-columns', filename)


def basement_joists_image_file_path(instance, filename):
    """Generate file path for new basement joists image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'basement-joists', filename)


def basement_subfloor_image_file_path(instance, filename):
    """Generate file path for new basement subfloor image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'basement-subfloor', filename)


def crawlspace_crawl_image_file_path(instance, filename):
    """Generate file path for new basement subfloor image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'crawlspace-crawl', filename)


def crawlspace_access_image_file_path(instance, filename):
    """Generate file path for new basement subfloor image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'crawlspace-access', filename)


def crawlspace_foundation_image_file_path(instance, filename):
    """Generate file path for new basement subfloor image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'crawlspace-foundation', filename)


def crawlspace_seismic_bolts_image_file_path(instance, filename):
    """Generate file path for new basement subfloor image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'crawlspace-seismic-bolts', filename)


def crawlspace_drainage_image_file_path(instance, filename):
    """Generate file path for new basement subfloor image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'crawlspace-drainage', filename)


def crawlspace_girders_beams_image_file_path(instance, filename):
    """Generate file path for new basement subfloor image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'crawlspace-girders-beams', filename)


def crawlspace_ventilation_image_file_path(instance, filename):
    """Generate file path for new basement subfloor image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'crawlspace-ventilation', filename)


def crawlspace_joists_image_file_path(instance, filename):
    """Generate file path for new basement subfloor image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'crawlspace-joists', filename)


def crawlspace_insulation_image_file_path(instance, filename):
    """Generate file path for new basement subfloor image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'crawlspace-insulation', filename)


def crawlspace_subfloor_image_file_path(instance, filename):
    """Generate file path for new basement subfloor image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'crawlspace-subfloor', filename)


def crawlspace_vapor_barrier_image_file_path(instance, filename):
    """Generate file path for new basement subfloor image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'crawlspace-vapor-barrier', filename)


def crawlspace_floor_image_file_path(instance, filename):
    """Generate file path for new basement subfloor image."""
    ext = os.path.splitext(filename)[1]
    filename = f'{uuid.uuid4()}{ext}'

    return os.path.join('uploads', 'crawlspace-floor', filename)


class UserManager(BaseUserManager):
    """Manager for users."""

    def create_user(self, email, password=None, **extra_fields):
        """Create, save and return a new user."""
        if not email:
            raise ValueError('User must have an email address.')
        user = self.model(email=self.normalize_email(email), **extra_fields)
        user.set_password(password)
        models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, password):
        """Create and return a new superuser."""
        user = self.create_user(email, password)
        models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser, PermissionsMixin):
    """User in the system."""
    user_id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        unique=True,
        )
    email = models.EmailField(max_length=255, unique=True)
    fname = models.CharField(max_length=25)
    lname = models.CharField(max_length=25)
    user_phone_number = models.CharField(max_length=25)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    objects = UserManager()
    USERNAME_FIELD = 'email'


class CompanyDetails(models.Model):
    """Model for company details"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    user_phone_number = models.CharField(max_length=25)
    company_name = models.CharField(max_length=75)
    company_addresses = models.CharField(max_length=255)
    company_emails = models.EmailField(max_length=255)
    company_phone_number = models.CharField(max_length=25)
    personal_address = models.CharField(max_length=125)
    licences_type = models.CharField(max_length=75)
    license_number = models.CharField(max_length=75)


class CompanyLogo(models.Model):
    """Company Logo Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    logo = models.ImageField(
        null=True,
        upload_to=logo_image_file_path
    )


class CompanySignature(models.Model):
    """Company Signature Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    signature = models.ImageField(
        null=True,
        upload_to=signature_image_file_path
    )


class ReportDetails(models.Model):
    """Report object."""
    report_uuid = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        unique=True,
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=100)
    r_id = models.CharField(max_length=100)
    address = models.CharField(max_length=175)
    date = models.DateTimeField(auto_now_add=True)
    client_name = models.CharField(max_length=25)
    bedroom_count = models.SmallIntegerField(default=0)
    bathroom_count = models.SmallIntegerField(default=0)
    garage_type = models.CharField(max_length=10)
    basement_type = models.CharField(max_length=20)

    def __str__(self):
        return self.title


class ReportDetailsPhoto(models.Model):
    """Report Details Photo Model"""

    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    report_details_photo = models.ImageField(
        null=True,
        upload_to=report_details_image_file_path)


class Overview(models.Model):
    """Overview Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    scope = models.CharField(max_length=1000)
    state_of_occupancy = models.CharField(max_length=15)
    weather = models.CharField(max_length=15)
    recent_rain = models.CharField(max_length=10)
    ground_cover = models.CharField(max_length=15)
    approx_age = models.CharField(max_length=10)


class Summary(models.Model):
    """Summary Model"""
    report_uuid = models.OneToOneField(
        ReportDetails,
        on_delete=models.CASCADE,
        verbose_name=('report_uuid'),
        primary_key=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    major_concerns = ArrayField(
        models.TextField(null=True, blank=True))
    safety_hazards = ArrayField(
        models.TextField(null=True, blank=True))
    further_review = ArrayField(
        models.TextField(null=True, blank=True))
    monitor = ArrayField(
        models.TextField(null=True, blank=True))
    general_maintenance = ArrayField(
        models.TextField(null=True, blank=True))
    needing_repair = ArrayField(
        models.TextField(null=True, blank=True))
    for_your_info = ArrayField(
        models.TextField(null=True, blank=True))


class ReceiptInvoice(models.Model):
    """Receipt/Invoice Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    company = models.CharField(max_length=75)
    date = models.DateTimeField('date published')
    inspector_fname = models.CharField(max_length=25)
    inspector_lname = models.CharField(max_length=25)
    client_name = models.CharField(max_length=25)
    payment_type = models.CharField(max_length=15)
    total_fee = models.DecimalField(max_digits=5, decimal_places=2)



###############################################################
# Uploading photos models
###############################################################

class PhotoAndComments(models.Model):
    uuid = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        unique=True,
    )
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    # user = models.ForeignKey(
    #     settings.AUTH_USER_MODEL,
    #     on_delete=models.CASCADE,
    # )
    photo = models.ImageField(upload_to=get_image_path, null=True, blank=True)
    comments = ArrayField(models.TextField(null=True, blank=True), null=True, blank=True)


class GenerateReport(models.Model):
    uuid = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        unique=True,
    )
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    # format = models.TextField(null=True, blank=True, default="pdf")





class GroundsSideWalksPhotos(models.Model):
    """Grounds Sidewalks Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    grounds_sidewalk_photos = models.ImageField(
        null=True,
        upload_to=grounds_sidewalk_image_file_path
    )


class GroundsDrivewayPhotos(models.Model):
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    grounds_driveway_photos = models.ImageField(
        null=True,
        upload_to=grounds_driveway_image_file_path
    )


class GroundsStoopStepsPhotos(models.Model):
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    grounds_stoop_steps_photos = models.ImageField(
        null=True,
        upload_to=grounds_stoop_steps_image_file_path
    )


class GroundsPatioPhotos(models.Model):
    """Grounds Patio Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    grounds_patio_photos = models.ImageField(
        null=True,
        upload_to=grounds_patio_image_file_path
    )


class GroundsPorchPhotos(models.Model):
    """Grounds Porch Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    grounds_porch_photos = models.ImageField(
        null=True,
        upload_to=grounds_porch_image_file_path
    )


class GroundsDeckPhotos(models.Model):
    """Grounds Deck Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    grounds_deck_photos = models.ImageField(
        null=True,
        upload_to=grounds_deck_image_file_path
    )


class GroundsCoversPhotos(models.Model):
    """Grounds Covers Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    grounds_covers_photos = models.ImageField(
        null=True,
        upload_to=grounds_covers_image_file_path
    )


class GroundsFencePhotos(models.Model):
    """Grounds Fence Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    grounds_fence_photos = models.ImageField(
        null=True,
        upload_to=grounds_fence_image_file_path
    )


class GroundsRetainingWallPhotos(models.Model):
    """Grounds Retaining Wall Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    grounds_retaining_wall_photos = models.ImageField(
        null=True,
        upload_to=grounds_retaining_wall_image_file_path
    )


class GroundsLandscapingPhotos(models.Model):
    """Grounds Landscaping Photos"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    grounds_landscaping_photos = models.ImageField(
        null=True,
        upload_to=grounds_landscaping_image_file_path
    )


class GroundsHoseBibsPhotos(models.Model):
    """Grounds Hose Bibs Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    grounds_hose_bibs_photos = models.ImageField(
        null=True,
        upload_to=grounds_hose_bibs_image_file_path
    )


class ExteriorChimneyPhotos(models.Model):
    """Exterior Chimney Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    exterior_chimney_photos = models.ImageField(
        null=True,
        upload_to=exterior_chimney_image_file_path
    )


class ExteriorGuttersPhotos(models.Model):
    """Exterior Gutters Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    exterior_gutters_photos = models.ImageField(
        null=True,
        upload_to=exterior_gutters_image_file_path
    )


class ExteriorSidingPhotos(models.Model):
    """Exterior Siding Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    exterior_siding_photos = models.ImageField(
        null=True,
        upload_to=exterior_siding_image_file_path
    )


class ExteriorTrimPhotos(models.Model):
    """Exterior Trim Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    exterior_trim_photos = models.ImageField(
        null=True,
        upload_to=exterior_trim_image_file_path
    )


class ExteriorSoffitPhotos(models.Model):
    """Exterior Soffit Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    exterior_soffit_photos = models.ImageField(
        null=True,
        upload_to=exterior_soffit_image_file_path
    )


class ExteriorFasciaPhotos(models.Model):
    """Exterior Chimney Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    exterior_fascia_photos = models.ImageField(
        null=True,
        upload_to=exterior_fascia_image_file_path
    )


class ExteriorFlashingPhotos(models.Model):
    """Exterior Flashing Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    exterior_flashing_photos = models.ImageField(
        null=True,
        upload_to=exterior_flashing_image_file_path
    )


class ExteriorCaulkingPhotos(models.Model):
    """Exterior Caulking Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    exterior_caulking_photos = models.ImageField(
        null=True,
        upload_to=exterior_caulking_image_file_path
    )


class ExteriorWindowsPhotos(models.Model):
    """Exterior Chimney Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    exterior_windows_photos = models.ImageField(
        null=True,
        upload_to=exterior_windows_image_file_path
    )


class ExteriorElectricalPhotos(models.Model):
    """Exterior Electrical Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    exterior_electrical_photos = models.ImageField(
        null=True,
        upload_to=exterior_electrical_image_file_path
    )


class ExteriorDoorsPhotos(models.Model):
    """Exterior Chimney Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    exterior_doors_photos = models.ImageField(
        null=True,
        upload_to=exterior_doors_image_file_path
    )


class ExteriorSlabOnGradePhotos(models.Model):
    """Exterior SlabOnGradePhotos"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    exterior_slab_on_grade_photos = models.ImageField(
        null=True,
        upload_to=exterior_slab_on_grade_image_file_path
    )


class ExteriorCoolingUnitPhotos(models.Model):
    """Exterior Cooling Unit Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    exterior_cooling_unit_photos = models.ImageField(
        null=True,
        upload_to=exterior_cooling_unit_image_file_path
    )


class RoofGeneralPhotos(models.Model):
    """Roof General Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    roof_general_photos = models.ImageField(
        null=True,
        upload_to=roof_general_image_file_path
    )


class RoofStylePhotos(models.Model):
    """Roof Style Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    roof_style_photos = models.ImageField(
        null=True,
        upload_to=roof_style_image_file_path
    )


class RoofVentilationPhotos(models.Model):
    """Roof Ventilation Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    roof_ventilation_photos = models.ImageField(
        null=True,
        upload_to=roof_ventilation_image_file_path
    )


class RoofFlashingPhotos(models.Model):
    """Roof Flashing Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    roof_flashing_photos = models.ImageField(
        null=True,
        upload_to=roof_flashing_image_file_path
    )


class RoofValleysPhotos(models.Model):
    """Roof Valleys Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    roof_valleys_photos = models.ImageField(
        null=True,
        upload_to=roof_valleys_image_file_path
    )


class RoofConditionPhotos(models.Model):
    """Roof Condition Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    roof_condition_photos = models.ImageField(
        null=True,
        upload_to=roof_condition_image_file_path
    )


class RoofSkylightsPhotos(models.Model):
    """Roof Skylights Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    roof_skylights_photos = models.ImageField(
        null=True,
        upload_to=roof_skylights_image_file_path
    )


class RoofPlumbingVentsPhotos(models.Model):
    """Roof General Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    roof_plumbing_vents_photos = models.ImageField(
        null=True,
        upload_to=roof_plumbing_vents_image_file_path
    )


class GarageTypePhotos(models.Model):
    """Garage Type Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    garage_type_photos = models.ImageField(
        null=True,
        upload_to=garage_type_image_file_path
    )


class GarageAutomaticOpenerPhotos(models.Model):
    """Garage Automatic Opener Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    garage_automatic_opener_photos = models.ImageField(
        null=True,
        upload_to=garage_automatic_opener_image_file_path
    )


class GarageSafetyReversePhotos(models.Model):
    """Garage Safety Reverse Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    garage_safety_reverse_photos = models.ImageField(
        null=True,
        upload_to=garage_safety_reverse_image_file_path
    )


class GarageRoofingPhotos(models.Model):
    """Garage Roofing Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    garage_roofing_photos = models.ImageField(
        null=True,
        upload_to=garage_roofing_image_file_path
    )


class GarageGuttersPhotos(models.Model):
    """Garage Type Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    garage_gutters_photos = models.ImageField(
        null=True,
        upload_to=garage_gutters_image_file_path
    )


class GarageSidingPhotos(models.Model):
    """Garage Siding Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    garage_siding_photos = models.ImageField(
        null=True,
        upload_to=garage_siding_image_file_path
    )


class GarageTrimPhotos(models.Model):
    """Garage Trim Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    garage_trim_photos = models.ImageField(
        null=True,
        upload_to=garage_trim_image_file_path
    )


class GarageExteriorDoorPhotos(models.Model):
    """Garage Exterior Door Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    garage_exterior_door_photos = models.ImageField(
        null=True,
        upload_to=garage_exterior_door_image_file_path
    )


class GarageOverheadDoorPhotos(models.Model):
    """Garage Overhead Door Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    garage_overhead_door_photos = models.ImageField(
        null=True,
        upload_to=garage_overhead_doors_image_file_path
    )


class GarageFloorPhotos(models.Model):
    """Garage Floor Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    garage_floor_photos = models.ImageField(
        null=True,
        upload_to=garage_flooring_image_file_path
    )


class GarageSillPlatesPhotos(models.Model):
    """Garage Sill Plates Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    garage_sill_plates_photos = models.ImageField(
        null=True,
        upload_to=garage_sill_plates_image_file_path
    )


class GarageElectricalPhotos(models.Model):
    """Garage Electrical Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    garage_electrical_photos = models.ImageField(
        null=True,
        upload_to=garage_electrical_image_file_path
    )


class GarageFireSeperationPhotos(models.Model):
    """Garage Fire Seperation Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    garage_fire_seperation_photos = models.ImageField(
        null=True,
        upload_to=garage_fire_seperation_image_file_path
    )


class PlumbingWaterServicePhotos(models.Model):
    """Plumbing Water Service Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    plumbing_water_service_photos = models.ImageField(
        null=True,
        upload_to=plumbing_water_service_image_file_path
    )


class PlumbingGasShutoffPhotos(models.Model):
    """Plumbing Gas Shutoff Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    plumbing_gas_shutoff_photos = models.ImageField(
        null=True,
        upload_to=plumbing_gas_shutoff_image_file_path
    )


class PlumbingWaterHeaterPhotos(models.Model):
    """Plumbing Water Heater Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    plumbing_water_heater_photos = models.ImageField(
        null=True,
        upload_to=plumbing_water_heater_image_file_path
    )


class PlumbingWellPumpPhotos(models.Model):
    """Plumbing Water Heater Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    plumbing_well_pump_photos = models.ImageField(
        null=True,
        upload_to=plumbing_well_pump_image_file_path
    )


class PlumbingSanitaryPumpPhotos(models.Model):
    """Plumbing Water Heater Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    plumbing_sanitary_pump_photos = models.ImageField(
        null=True,
        upload_to=plumbing_sanitary_pump_image_file_path
    )


class BasementStairsPhotos(models.Model):
    """Basement Stairs Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    basement_stairs_photos = models.ImageField(
        null=True,
        upload_to=basement_stairs_image_file_path
    )


class BasementFoundationPhotos(models.Model):
    """Basement Foundation Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    basement_foundation_photos = models.ImageField(
        null=True,
        upload_to=basement_foundation_image_file_path
    )


class BasementDrainagePhotos(models.Model):
    """Basement Drainage Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    basement_drainage_photos = models.ImageField(
        null=True,
        upload_to=basement_drainage_image_file_path
    )


class BasementColumnsPhotos(models.Model):
    """Basement Columns Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    basement_columns_photos = models.ImageField(
        null=True,
        upload_to=basement_columns_image_file_path
    )


class BasementSeismicBoltsPhotos(models.Model):
    """Basement Seismic Bolts Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    basement_seismic_bolts_photos = models.ImageField(
        null=True,
        upload_to=basement_seismic_bolts_image_file_path
    )


class BasementFloorPhotos(models.Model):
    """Basement Floor Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    basement_floor_photos = models.ImageField(
        null=True,
        upload_to=basement_floor_image_file_path
    )


class BasementGirdersBeamsPhotos(models.Model):
    """Basement Floor Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    basement_girders_beams_photos = models.ImageField(
        null=True,
        upload_to=basement_girders_beams_image_file_path
    )


class BasementJoistsPhotos(models.Model):
    """Basement Joists Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    basement_joists_photos = models.ImageField(
        null=True,
        upload_to=basement_joists_image_file_path
    )


class BasementSubfloorPhotos(models.Model):
    """Basement Stairs Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    basement_subfloor_photos = models.ImageField(
        null=True,
        upload_to=basement_subfloor_image_file_path
    )


class CrawlspaceCrawlPhotos(models.Model):
    """CrawlspaceCrawl Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    crawlspace_crawl_photos = models.ImageField(
        null=True,
        upload_to=crawlspace_crawl_image_file_path
    )


class CrawlspaceAccessPhotos(models.Model):
    """CrawlspaceCrawl Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    crawlspace_access_photos = models.ImageField(
        null=True,
        upload_to=crawlspace_access_image_file_path
    )


class CrawlspaceFoundationPhotos(models.Model):
    """CrawlspaceCrawl Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    crawlspace_foundation_photos = models.ImageField(
        null=True,
        upload_to=crawlspace_foundation_image_file_path
    )


class CrawlspaceFloorPhotos(models.Model):
    """CrawlspaceCrawl Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    crawlspace_floor_photos = models.ImageField(
        null=True,
        upload_to=crawlspace_floor_image_file_path
    )


class CrawlspaceDrainagePhotos(models.Model):
    """CrawlspaceCrawl Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    crawlspace_drainage_photos = models.ImageField(
        null=True,
        upload_to=crawlspace_drainage_image_file_path
    )


class CrawlspaceSeismicBoltsPhotos(models.Model):
    """CrawlspaceCrawl Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    crawlspace_seismic_bolts_photos = models.ImageField(
        null=True,
        upload_to=crawlspace_seismic_bolts_image_file_path
    )


class CrawlspaceGirdersBeamsPhotos(models.Model):
    """CrawlspaceCrawl Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    crawlspace_girders_beams_photos = models.ImageField(
        null=True,
        upload_to=crawlspace_girders_beams_image_file_path
    )


class CrawlspaceJoistsPhotos(models.Model):
    """CrawlspaceCrawl Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    crawlspace_joists_photos = models.ImageField(
        null=True,
        upload_to=crawlspace_joists_image_file_path
    )


class CrawlspaceSubfloorPhotos(models.Model):
    """CrawlspaceCrawl Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    crawlspace_subfloor_photos = models.ImageField(
        null=True,
        upload_to=crawlspace_subfloor_image_file_path
    )


class CrawlspaceVentilationPhotos(models.Model):
    """CrawlspaceCrawl Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    crawlspace_ventilation_photos = models.ImageField(
        null=True,
        upload_to=crawlspace_ventilation_image_file_path
    )


class CrawlspaceInsulationPhotos(models.Model):
    """CrawlspaceCrawl Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    crawlspace_insulation_photos = models.ImageField(
        null=True,
        upload_to=crawlspace_insulation_image_file_path
    )


class CrawlspaceVaporBarrierPhotos(models.Model):
    """CrawlspaceCrawl Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    crawlspace_vapor_barrier_photos = models.ImageField(
        null=True,
        upload_to=crawlspace_vapor_barrier_image_file_path
    )


class KitchenCountertopPhotos(models.Model):
    """Kitchen Countertop Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    kitchen_countertop_photos = models.ImageField(
        null=True,
        upload_to=kitchen_countertops_image_file_path
    )


class KitchenCabinetsPhotos(models.Model):
    """Kitchen Cabinets Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    kitchen_cabinets_photos = models.ImageField(
        null=True,
        upload_to=kitchen_cabinets_image_file_path
    )


class KitchenPlumbingPhotos(models.Model):
    """Kitchen Plumbing Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    kitchen_plumbing_photos = models.ImageField(
        null=True,
        upload_to=kitchen_plumbing_image_file_path
    )


class KitchenFloorPhotos(models.Model):
    """Kitchen Floor Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    kitchen_floor_photos = models.ImageField(
        null=True,
        upload_to=kitchen_floor_image_file_path
    )


class KitchenWallsCeilingPhotos(models.Model):
    """Kitchen Floor Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    kitchen_walls_ceiling_photos = models.ImageField(
        null=True,
        upload_to=kitchen_walls_ceiling_image_file_path
    )


class KitchenAppliancesPhotos(models.Model):
    """Kitchen Floor Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    kitchen_appliances_photos = models.ImageField(
        null=True,
        upload_to=kitchen_appliances_image_file_path
    )


class LaundryPhotos(models.Model):
    """Laundry Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    laundry_photos = models.ImageField(
        null=True,
        upload_to=laundry_image_file_path
    )


class BathroomElectricalPhotos(models.Model):
    """Bathroom Electrical Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    bathroom_electrical_photos = models.ImageField(
        null=True,
        upload_to=bathroom_electrical_image_file_path
    )


class BathroomPlumbingPhotos(models.Model):
    """Bathroom Electrical Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    bathroom_plumbing_photos = models.ImageField(
        null=True,
        upload_to=bathroom_plumbing_image_file_path
    )


class BedroomPhotos(models.Model):
    """Bedroom Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    bedroom_photos = models.ImageField(
        null=True,
        upload_to=bedroom_image_file_path
    )


class DiningRoomPhotos(models.Model):
    """Dining Room Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    dining_room_photos = models.ImageField(
        null=True,
        upload_to=dining_room_image_file_path
    )


class InteriorFireplacePhotos(models.Model):
    """Interior Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    interior_fireplace_photos = models.ImageField(
        null=True,
        upload_to=interior_fireplace_image_file_path
    )


class InteriorStepsPhotos(models.Model):
    """Interior Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    interior_steps_photos = models.ImageField(
        null=True,
        upload_to=interior_steps_image_file_path
    )


class InteriorSmokeCarbonDetPhotos(models.Model):
    """Interior Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    interior_smoke_carbon_det_photos = models.ImageField(
        null=True,
        upload_to=interior_smoke_carbon_det_image_file_path
    )


class InteriorAtticPhotos(models.Model):
    """Interior Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    interior_attic_photos = models.ImageField(
        null=True,
        upload_to=interior_attic_image_file_path
    )


class FurnacePhotos(models.Model):
    """Furnace Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    furnace_photos = models.ImageField(
        null=True,
        upload_to=furnace_image_file_path
    )


class BoilerPhotos(models.Model):
    """Boiler Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    boiler_photos = models.ImageField(
        null=True,
        upload_to=boiler_image_file_path
    )


class HeatingOtherSystemsPhotos(models.Model):
    """Heating Systems Other Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    heating_other_systems_photos = models.ImageField(
        null=True,
        upload_to=heating_other_systems_image_file_path
    )


class EvaporatorCoilPhotos(models.Model):
    """Heating Systems Other Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    evaporator_coil_photos = models.ImageField(
        null=True,
        upload_to=evaporator_coil_file_path
    )


class LivingRoomPhotos(models.Model):
    """Living Room Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    living_room_photos = models.ImageField(
        null=True,
        upload_to=living_room_image_file_path
    )


class ElectricalMainPanelPhotos(models.Model):
    """Living Room Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    electrical_main_panel_photos = models.ImageField(
        null=True,
        upload_to=main_panel_image_file_path
    )


class ElectricalSubPanelPhotos(models.Model):
    """Living Room Photos Model"""
    report_uuid = models.ForeignKey(
        ReportDetails,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    electrical_sub_panel_photos = models.ImageField(
        null=True,
        upload_to=main_panel_image_file_path
    )


class UserCommentsSummary(models.Model):
    """All comments Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    type = models.CharField(max_length=55)
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)



class GroundsDrivewaySummary(models.Model):
    """Grounds Driveway Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GroundsSidewalkSummary(models.Model):
    """Grounds Sidewalk Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GroundsStoopStepsSummary(models.Model):
    """Grounds Stoop/Steps Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GroundsPatioSummary(models.Model):
    """Grounds Driveway Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GroundsPorchSummary(models.Model):
    """Grounds Porch Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GroundsDeckSummary(models.Model):
    """Grounds Deck Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GroundsCoversSummary(models.Model):
    """Grounds Covers Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GroundsFenceSummary(models.Model):
    """Grounds Fence Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GroundsLandscapingSummary(models.Model):
    """Grounds Driveway Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GroundsHoseBibsSummary(models.Model):
    """Grounds Hose Bibs Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GroundsRetainingWallSummary(models.Model):
    """Grounds Retaining Wall Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class RoofGeneralSummary(models.Model):
    """Roof General Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class RoofStyleSummary(models.Model):
    """Roof Style Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class RoofVentilationSummary(models.Model):
    """Roof Ventilation Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class RoofFlashingSummary(models.Model):
    """Roof Flashing Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class RoofValleysSummary(models.Model):
    """Roof Valleys Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class RoofConditionSummary(models.Model):
    """Roof Condition Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class RoofSkylightsSummary(models.Model):
    """Roof Skylights Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class RoofPlumbingVentsSummary(models.Model):
    """Roof Plumbing Vents Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class ExteriorChimneySummary(models.Model):
    """Exterior Chimney Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class ExteriorSidingSummary(models.Model):
    """Exterior Siding Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class ExteriorTrimSummary(models.Model):
    """Exterior Trim Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class ExteriorSoffitSummary(models.Model):
    """Exterior Soffit Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class ExteriorFlashingSummary(models.Model):
    """Exterior Flashing Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class ExteriorFasciaSummary(models.Model):
    """Exterior Fascia Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class ExteriorGuttersSummary(models.Model):
    """Exterior Gutters Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class ExteriorWindowsSummary(models.Model):
    """Exterior Windows Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class ExteriorDoorsSummary(models.Model):
    """Exterior Doors Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class ExteriorServiceEntrySummary(models.Model):
    """Exterior Doors Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class ExteriorWallConstructionSummary(models.Model):
    """Exterior Doors Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class ExteriorSlabOnFoundationSummary(models.Model):
    """Exterior Slab on Grade Foundation Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class ExteriorCaulkingSummary(models.Model):
    """Exterior Caulking Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GarageTypeSummary(models.Model):
    """Garage Type Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GarageAutomaticOpenerSummary(models.Model):
    """Garage Automatic Opener Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GarageSafetyReverseSummary(models.Model):
    """Garage Safety Reverse Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GarageGuttersSummary(models.Model):
    """Garage Gutters Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GarageRoofingSummary(models.Model):
    """Garage Roofing Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GarageSidingSummary(models.Model):
    """Garage Siding Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GarageFloorSummary(models.Model):
    """Garage Floor Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GarageSillPlatesSummary(models.Model):
    """Garage Sill Plates Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GarageOverheadDoorSummary(models.Model):
    """Garage Type Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GarageExteriorDoorSummary(models.Model):
    """Garage Exterior Door Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GarageElectricalSummary(models.Model):
    """Garage Electrical Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class GarageFireSeperationSummary(models.Model):
    """Garage Type Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class BasementStairsSummary(models.Model):
    """Basement Stairs Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class BasementFoundationSummary(models.Model):
    """Basement Foundation Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class BasementFloorSummary(models.Model):
    """Basement Floor Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class BasementDrainageSummary(models.Model):
    """Basement Drainage Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class BasementColumnsSummary(models.Model):
    """Basement Columns Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class BasementSeismicBoltsSummary(models.Model):
    """Basement Seismic Bolts Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class BasementGirdersBeamsSummary(models.Model):
    """Basement Girders/Beams Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class BasementJoistsSummary(models.Model):
    """Basement Joists Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class BasementSubfloorSummary(models.Model):
    """Basement Subfloor Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class CrawlspaceCrawlSummary(models.Model):
    """Crawlspace Crawl Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class CrawlspaceAccessSummary(models.Model):
    """Crawlspace Access Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class CrawlspaceVentilationSummary(models.Model):
    """Crawlspace Ventilation Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class CrawlspaceInsulationSummary(models.Model):
    """Crawlspace Insulation Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class CrawlspaceVaporBarrierSummary(models.Model):
    """Crawlspace Vapor Barrier Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class KitchenCountertopSummary(models.Model):
    """Kitchen Countertop Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class KitchenCabinetsSummary(models.Model):
    """Kitchen Cabinets Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class KitchenPlumbingSummary(models.Model):
    """Kitchen Countertop Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class KitchenFloorSummary(models.Model):
    """Kitchen Floor Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class KitchenWallsCeilingSummary(models.Model):
    """Kitchen Walls/Ceiling Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class KitchenAppliancesSummary(models.Model):
    """Kitchen Appliances Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class BathroomPlumbingSummary(models.Model):
    """Bathroom Plumbing Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class BathroomElectricalSummary(models.Model):
    """Bathroom Electrical Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class InteriorFireplaceSummary(models.Model):
    """Interior Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class InteriorStepsSummary(models.Model):
    """Interior Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class InteriorSmokeCarbonDetSummary(models.Model):
    """Interior Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class InteriorAtticSummary(models.Model):
    """Interior Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class PlumbingWaterServiceSummary(models.Model):
    """Plumbing Water Service Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class PlumbingGasShutoffSummary(models.Model):
    """Plumbing Gas Shutoff Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class PlumbingWaterHeaterSummary(models.Model):
    """Plumbing Water Heater Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class ElectricalMainPanelSummary(models.Model):
    """Electrical Main Panel Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class ElectricalSubPanelSummary(models.Model):
    """Electrical Sub Panel Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


class BedroomSummary(models.Model):
    """Bedroom Summary Model"""
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=55)
    comment = models.CharField(max_length=2000)


###############################################################################
# Create custom Classes that we will be using to avoid repetations
# At the moment I am not using this, I went with the metaClasses

class CustomItemField(ArrayField):
      def __init__(self, **kwargs):
        kwargs['null'] = False
        kwargs['blank'] = False
        kwargs['base_field'] = models.TextField(null=True, blank=True)
        super().__init__(**kwargs)

class CustomCommentField(ArrayField):
    def __init__(self, **kwargs):
        kwargs['null'] = True
        kwargs['blank'] = True
        kwargs['base_field'] = models.TextField(null=True, blank=True)
        super().__init__(**kwargs)

class CustomPhotoField(ArrayField):
    def __init__(self, **kwargs):
        kwargs['null'] = True
        kwargs['blank'] = True
        kwargs['base_field'] = models.TextField(null=True, blank=True)
        super().__init__(**kwargs)



###############################################################################
# Create custom Meta Class to add items to the class dynamically
class CustomMetaclass(models.base.ModelBase):
    def __new__(mcs, name, bases, attrs):
        # add the default report_uuid and user
        attrs['report_uuid'] = models.ForeignKey(
            ReportDetails,
            on_delete=models.CASCADE,
            null=True
        )
        attrs['user'] = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            on_delete=models.CASCADE,
        )

        # then add the rest of the fields including their comments and photos
        fields = attrs.get('fields', [])
        for field in fields:
            attrs[field] = ArrayField(models.TextField(null=True, blank=True), null=True, blank=True)
            # this is the comments to a specific photo -- likely patched
            attrs[f'{field}_photo_uuid'] = ArrayField(models.TextField(null=True, blank=True), null=True, blank=True)
            # below are the general comments for a section
            attrs[f'{field}_comments'] = ArrayField(models.TextField(null=True, blank=True), null=True, blank=True)
        return super().__new__(mcs, name, bases, attrs)


###############################################################################
###############################################################################
# Sections Models
# Let's remove some repetations here by using a metaClass and just defining the
# columns we need in our databases
# Joseph ~ fiverr
###############################################################################
###############################################################################


class Grounds(models.Model, metaclass=CustomMetaclass):
    """Grounds Model"""
    fields = [
        'service_walks',
        'drive_parking',
        'stoop_steps',
        'patio',
        'deck_balcony',
        'covers',
        'fence_wall',
        'landscaping',
        'retaining_wall',
        'hose_bibs'
    ]


class Roof(models.Model, metaclass=CustomMetaclass):
    """Roof Model"""
    fields = [
        'general',
        'style',
        'ventilation',
        'flashing',
        'valleys',
        'condition',
        'skylights',
        'plumbing_vents'
    ]


class Exterior(models.Model, metaclass=CustomMetaclass):
    """Exterior Model"""
    fields = [
        'chimney',
        'gutters',
        'siding',
        'trim',
        'soffit',
        'fascia',
        'flashing',
        'caulking',
        'windows',
        'slab_on_foundation',
        'service_entry',
        'wall_construction',
        'exterior_doors',
    ]

class GarageCarport(models.Model, metaclass=CustomMetaclass):
    """Garage Model"""
    fields = [
        'type',
        'automatic_opener',
        'safety_reverse',
        'roofing',
        'gutters',
        'siding',
        'trim',
        'floor',
        'sillplate',
        'overhead_doors',
        'service_door',
        'electrical',
        'walls_ceiling',

    ]

class Kitchen(models.Model, metaclass=CustomMetaclass):
    """Kitchen Model"""
    fields = [
        'countertops',
        'cabinets',
        'plumbing',
        'walls_ceiling',
        'heating_cooling',
        'floor',
        'appliances',
    ]

class Laundry(models.Model, metaclass=CustomMetaclass):
    """Laundry Model"""
    fields = [
        'laundry',
    ]

class Bathroom(models.Model, metaclass=CustomMetaclass):
    """Bathroom Plumbing Model"""
    fields = [
        'bathroom_plumbing',
        'bathroom_interior',
    ]

class Bedrooms(models.Model, metaclass=CustomMetaclass):
    """Bedrooms Model"""
    fields = [
        'bedroom',
    ]

class Interior(models.Model, metaclass=CustomMetaclass):
    """Interior Model"""
    fields = [
        'fireplace',
        'stairs_steps',
        'smoke_carbon_det',
        'attic',
    ]

class Basement(models.Model, metaclass=CustomMetaclass):
    """Basement Model"""
    fields = [
        'stairs',
        'foundation',
        'floor',
        'seismic_bolts',
        'drainage',
        'girders_beams',
        'columns',
        'joists',
        'subfloor',
    ]

class Crawlspace(models.Model, metaclass=CustomMetaclass):
    """Crawlspace Model"""
    fields = [
        'crawlspace',
        'access',
        'foundation',
        'floor',
        'seismic_bolts',
        'drainage',
        'ventilation',
        'girders_beams',
        'joists',
        'subfloor',
        'insulation',
        'vapor_barriers',
    ]

class Plumbing(models.Model, metaclass=CustomMetaclass):
    """Plumbing Model"""
    fields = [
        'water_service',
        'fuel_shutoff',
        'well_pump',
        'sanitary_pump',
        'water_heater',
    ]

class HeatingSystem(models.Model, metaclass=CustomMetaclass):
    """Heating System Model"""
    fields = [
        'other_systems',
        'furnace_unit',
        'boiler_unit',
        'evap_coil',
        'exterior_cooling_unit',
    ]

class ElectricalCoolingSystems(models.Model, metaclass=CustomMetaclass):
    """Electrical and Cooling Systems Model"""
    fields = [
        'main_panel',
        'sub_panel',
    ]

class LivingRoom(models.Model, metaclass=CustomMetaclass):
    """Living Room Model"""
    fields = [
        'living_room',
    ]

class DiningRoom(models.Model, metaclass=CustomMetaclass):
    """Dining Room Model"""
    fields = [
        'dining_room',
    ]

