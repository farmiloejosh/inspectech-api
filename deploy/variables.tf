variable "prefix" {
  default = "iad"
}

variable "project" {
  default = "inspectech-api"
}

variable "contact" {
  default = "farmiloe.josh@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "ainspectpro-api-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "134289158770.dkr.ecr.us-east-1.amazonaws.com/inspectech-api:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "134289158770.dkr.ecr.us-east-1.amazonaws.com/inspectech-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "ainspectprorestapinafkjsdfl.link"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
