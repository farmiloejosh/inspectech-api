"""
Django admin customization.
"""
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import gettext_lazy as _

from core import models
from core.models import (
    Bathroom, GroundsDrivewaySummary, GarageAutomaticOpenerSummary,
    GarageElectricalSummary, GarageExteriorDoorSummary, GroundsCoversSummary,
    GroundsDeckSummary, GroundsFenceSummary, GroundsHoseBibsSummary,
    GroundsLandscapingSummary, GroundsPatioSummary,
    GroundsRetainingWallSummary, LaundryPhotos,
    GroundsSidewalkSummary, GroundsStoopStepsSummary, ExteriorCaulkingSummary,
    ExteriorChimneySummary, ExteriorDoorsSummary, ExteriorFasciaSummary,
    ExteriorFlashingSummary, ExteriorGuttersSummary, ExteriorSidingSummary,
    ExteriorSlabOnFoundationSummary, ExteriorSoffitSummary,
    ExteriorTrimSummary, DiningRoomPhotos, ReportDetailsPhoto,
    ExteriorWindowsSummary, RoofConditionSummary, RoofFlashingSummary,
    RoofGeneralSummary, RoofPlumbingVentsSummary, RoofSkylightsSummary,
    RoofStyleSummary, RoofValleysSummary, RoofVentilationSummary,
    BasementColumnsSummary, BasementDrainageSummary, BasementFloorSummary,
    BasementFoundationSummary, BasementGirdersBeamsSummary,
    BasementJoistsSummary, LivingRoomPhotos,
    BasementSeismicBoltsSummary, InteriorAtticSummary,
    BasementStairsSummary, BasementSubfloorSummary,
    GarageFireSeperationSummary, GarageFloorSummary, GarageGuttersSummary,
    GarageOverheadDoorSummary, GarageSafetyReverseSummary,
    GarageSidingSummary, GarageSillPlatesSummary,
    GarageTypeSummary, KitchenAppliancesSummary, KitchenCabinetsSummary,
    KitchenCountertopSummary, KitchenFloorSummary, KitchenPlumbingSummary,
    KitchenWallsCeilingSummary, BathroomElectricalSummary,
    BathroomPlumbingSummary, InteriorSmokeCarbonDetSummary,
    BedroomSummary, PlumbingGasShutoffSummary, InteriorStepsSummary,
    PlumbingWaterHeaterSummary, PlumbingWaterServiceSummary,
    ElectricalMainPanelSummary, InteriorFireplaceSummary,
    ElectricalSubPanelSummary, GroundsPorchSummary, CrawlspaceCrawlSummary,
    CrawlspaceAccessSummary, CrawlspaceInsulationSummary,
    CrawlspaceVaporBarrierSummary, ExteriorServiceEntrySummary,
    CrawlspaceVentilationSummary, ExteriorWallConstructionSummary,
    ReportDetails, Grounds, Roof, Exterior, HeatingSystem,
    GarageCarport, Kitchen, Basement, Crawlspace,
    Plumbing, ElectricalCoolingSystems, DiningRoom, Laundry,
    LivingRoom, Interior, Bathroom,
    Bedrooms,
    Overview, ReceiptInvoice, Summary,
)


class UserAdmin(BaseUserAdmin):
    """Define the admin pages for users."""
    ordering = ['user_id']
    list_display = ['email', 'lname']
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal Info'), {'fields': ('lname',)}),
        (
            _('Permissions'),
            {
                'fields': (
                    'is_active',
                    'is_staff',
                    'is_superuser',
                )
            }
        ),
        (_('Important dates'), {'fields': ('last_login',)}),
    )
    readonly_fields = ['last_login']
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'email',
                'password1',
                'password2',
                'lname',
                'is_active',
                'is_staff',
                'is_superuser',
            ),
        }),
    )


admin.site.register(models.User, UserAdmin)

admin.site.register(
    [
        GroundsDrivewaySummary, GarageAutomaticOpenerSummary,
        GarageElectricalSummary, GarageExteriorDoorSummary,
        GroundsCoversSummary,
        GroundsDeckSummary, GroundsFenceSummary, GroundsHoseBibsSummary,
        GroundsLandscapingSummary, GroundsPatioSummary,
        GroundsRetainingWallSummary, LaundryPhotos,
        GroundsSidewalkSummary, GroundsStoopStepsSummary,
        ExteriorCaulkingSummary,
        ExteriorChimneySummary, ExteriorDoorsSummary, ExteriorFasciaSummary,
        ExteriorFlashingSummary, ExteriorGuttersSummary, ExteriorSidingSummary,
        ExteriorSlabOnFoundationSummary, ExteriorSoffitSummary,
        ExteriorTrimSummary, DiningRoomPhotos, ReportDetailsPhoto,
        ExteriorWindowsSummary, RoofConditionSummary, RoofFlashingSummary,
        RoofGeneralSummary, RoofPlumbingVentsSummary, RoofSkylightsSummary,
        RoofStyleSummary, RoofValleysSummary, RoofVentilationSummary,
        BasementColumnsSummary, BasementDrainageSummary, BasementFloorSummary,
        BasementFoundationSummary, BasementGirdersBeamsSummary,
        BasementJoistsSummary, LivingRoomPhotos,
        BasementSeismicBoltsSummary, InteriorAtticSummary,
        BasementStairsSummary, BasementSubfloorSummary,
        GarageFireSeperationSummary, GarageFloorSummary, GarageGuttersSummary,
        GarageOverheadDoorSummary, GarageSafetyReverseSummary,
        GarageSidingSummary, GarageSillPlatesSummary,
        GarageTypeSummary, KitchenAppliancesSummary, KitchenCabinetsSummary,
        KitchenCountertopSummary, KitchenFloorSummary, KitchenPlumbingSummary,
        KitchenWallsCeilingSummary, BathroomElectricalSummary,
        BathroomPlumbingSummary, InteriorSmokeCarbonDetSummary,
        BedroomSummary, PlumbingGasShutoffSummary, InteriorStepsSummary,
        PlumbingWaterHeaterSummary, PlumbingWaterServiceSummary,
        ElectricalMainPanelSummary, InteriorFireplaceSummary,
        ElectricalSubPanelSummary, GroundsPorchSummary, CrawlspaceCrawlSummary,
        CrawlspaceAccessSummary, CrawlspaceInsulationSummary,
        CrawlspaceVaporBarrierSummary, ExteriorServiceEntrySummary,
        CrawlspaceVentilationSummary, ExteriorWallConstructionSummary,
        ReportDetails, Grounds, Roof, Exterior, HeatingSystem,
        GarageCarport, Kitchen, Basement, Crawlspace,
        Plumbing, ElectricalCoolingSystems, DiningRoom, Laundry,
        LivingRoom, Interior, Bathroom,
        Bedrooms,
        Overview, ReceiptInvoice, Summary,
    ]
)
