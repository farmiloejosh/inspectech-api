from rest_framework import serializers
from core.models import (
    CompanyDetails,
    CompanyLogo,
    CompanySignature
)


class CompanyDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompanyDetails
        fields = ['user', 'user_phone_number',
                  'company_name', 'licences_type',
                  'company_addresses', 'company_emails',
                  'company_phone_number', 'personal_address',
                  'license_number']
        read_only_fields = ['user']


class CompanyLogoSerializer(serializers.ModelSerializer):
    """Serializer for uploading images to recipes."""

    class Meta:
        model = CompanyLogo
        fields = ['id', 'logo']
        read_only_fields = ['id']
        extra_kwargs = {'logo': {'required': 'False'}}


class CompanySignatureSerializer(serializers.ModelSerializer):
    """Serializer for uploading images to recipes."""

    class Meta:
        model = CompanySignature
        fields = ['id', 'signature']
        read_only_fields = ['id']
        extra_kwargs = {'signature': {'required': 'False'}}
