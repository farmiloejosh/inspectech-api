from rest_framework import (
    viewsets,
    status,
)
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from company import serializers

from core.models import (
    CompanyDetails,
    CompanyLogo,
    CompanySignature
)


class CompanyDetailsViewSet(viewsets.ModelViewSet):
    """View for manage report APIs."""
    serializer_class = serializers.CompanyDetailsSerializer
    queryset = CompanyDetails.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """Retrieve report details for authenticated user."""
        queryset = self.queryset
        return queryset.filter(
            user=self.request.user
            ).distinct()

    def get_serializer_class(self):
        """Return the serializer class for request."""
        if self.action == 'list':
            return serializers.CompanyDetailsSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new report."""
        serializer.save(user=self.request.user)


class CompanyLogoViewSet(viewsets.ModelViewSet):
    """Manage company logo in the database."""
    serializer_class = serializers.CompanyLogoSerializer
    queryset = CompanyLogo.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_company_logo_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'company_logo')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class CompanySignatureViewSet(viewsets.ModelViewSet):
    """Manage company logo in the database."""
    serializer_class = serializers.CompanySignatureSerializer
    queryset = CompanySignature.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @api_view(['POST', 'GET', 'DELETE'])
    def upload_company_signature_photo(self, request, pk=None):
        """Upload an image to report."""
        report = self.get_object()
        serializer = self.get_serializer(report, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """Filter queryset to authenticated user."""
        return self.queryset.filter(user=self.request.user).order_by(
            'company_signature')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
