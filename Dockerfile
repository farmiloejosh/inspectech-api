FROM python:3.9-slim

LABEL maintainer="farmiloej"

ENV PYTHONUNBUFFERED 1
ENV PATH="/scripts:${PATH}"

# Set up working directory
WORKDIR /app
COPY ./app /app
COPY ./scripts /scripts
RUN chmod +x /scripts/*

# Copy dependency files
COPY ./requirements.txt /tmp/requirements.txt
# COPY ./requirements.dev.txt /tmp/requirements.dev.txt

# Install necessary system packages
RUN apt-get update && \
    apt-get install -y \
    python3-pip \
    libpango-1.0-0 \
    libpangoft2-1.0-0 \
    libharfbuzz-subset0 \
    postgresql-client \
    libjpeg-dev \
    libpq-dev \
    && rm -rf /var/lib/apt/lists/*

# Create virtual environment and install Python packages
RUN python -m venv /py && \
    /py/bin/pip install --upgrade pip && \
    /py/bin/pip install -r /tmp/requirements.txt && \
    if [ "$DEV" = "true" ]; then \
        /py/bin/pip install -r /tmp/requirements.dev.txt; \
    fi && \
    rm -rf /tmp

# Set environment variables and user permissions
ENV PATH="/py/bin:$PATH"
RUN adduser --disabled-password --no-create-home django-user && \
    mkdir -p /vol/web/media && \
    mkdir -p /vol/web/static && \
    chown -R django-user:django-user /vol/ && \
    chmod -R 755 /vol/web

USER django-user
VOLUME /vol/web

# Command to run the application (override in development)
CMD ["/scripts/entrypoint.sh"]
