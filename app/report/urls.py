"""
Report Urls
"""
from django.urls import (
    path,
    include,
)

from rest_framework.routers import DefaultRouter

from report import views


router = DefaultRouter()
router.register(
    'report-details', views.ReportDetailsViewSet)
router.register(
    'report-details-photo', views.ReportDetailsPhotoViewSet)
router.register(
    'summary-section', views.SummarySectionViewSet)
router.register(
    'receipt-invoice-section', views.ReceiptInvoiceSectionViewSet)
router.register(
    'grounds-section', views.OverviewSectionViewSet)
router.register(
    'grounds-section', views.GroundsSectionViewSet)
router.register(
    'roof-section', views.RoofSectionViewSet)
router.register(
    'exterior-section', views.ExteriorSectionViewSet)
router.register(
    'garage-section', views.GarageSectionViewSet)
router.register(
    'kitchen-section', views.KitchenSectionViewSet)
router.register(
    'basement-section', views.BasementSectionViewSet)
router.register(
    'crawlspace-section',
    views.CrawlspaceSectionViewSet)
router.register(
    'plumbing-section', views.PlumbingSectionViewSet)
router.register(
    'electrical-section',
    views.ElectricalSectionViewSet)
router.register(
    'heating-systems-section',
    views.HeatingSystemsSectionViewSet)
router.register(
    'bathroom-electrical-section',
    views.BathroomSectionViewSet)
router.register(
    'interior-section', views.InteriorSectionViewSet)
router.register(
    'bedroom-section', views.BedroomSectionViewSet)
router.register(
    'living-room-section',
    views.LivingRoomSectionViewSet)
router.register(
    'dining-room-section',
    views.DiningRoomSectionViewSet)


################################################################################
################################################################################
#  Hey!! There was alot of comments url links which is way repetitive
#  I have replaced it with just one url and one Model that will handle all
#  comments of a given user
#  Incase you need the every comment routes, you can uncomment them below
#
#   This is the logic: With just One model, we add an extra column
#   that will store the type of comment e.g GroundsDriveway, GroundsPorch
#   With that just one model will handle for us all the user comments
#
#
#   @ Joseph ~ fiverr
################################################################################
################################################################################

router.register(
    'user-comments', views.UserCommentsSummaryViewSet)



################################################################################
################################################################################
#  Hey!! There was alot of photos links which is repetitive
#  I have replced it with just one url and one Model that will handle that
#  Incase you need the every photo routes, you can uncomment them below
#   @ Joseph ~ fiverr
################################################################################
################################################################################





router.register(
    'upload-photo-and-comments', views.PhotoAndCommentsViewSet)
router.register(
    'generate-report', views.GenerateReportViewSet)


# router.register(
#     'ground-driveway-photos', views.GroundsDrivewayPhotosViewSet)
# router.register(
#     'ground-sidewalk-photos', views.GroundsSidewalkPhotosViewSet)
# router.register(
#     'ground-stoop-steps-photos', views.GroundsStoopStepsPhotosViewSet)
# router.register(
#     'ground-porch-photos', views.GroundsPorchPhotosViewSet)
# router.register(
#     'ground-cover-photos', views.GroundsCoverPhotosView)
# router.register(
#     'ground-deck-photos', views.GroundsDeckPhotosViewSet)
# router.register(
#     'ground-patio-photos', views.GroundsPatioPhotosViewSet)
# router.register(
#     'ground-fence-photos', views.GroundsFencePhotosViewSet)
# router.register(
#     'ground-landscaping-photos', views.GroundsLandscapingPhotosViewSet)
# router.register(
#     'ground-hose-bibs-photos', views.GroundsHoseBibsPhotosViewSet)
# router.register(
#     'ground-retaining-wall-photos', views.GroundsRetainingWallPhotosViewSet)
# router.register(
#     'exterior-chimney-photos', views.ExteriorChimneyPhotosViewSet)
# router.register(
#     'exterior-siding-photos', views.ExteriorSidingPhotosViewSet)
# router.register(
#     'exterior-trim-photos', views.ExteriorTrimPhotosViewSet)
# router.register(
#     'exterior-flashing-photos', views.ExteriorFlashingPhotosViewSet)
# router.register(
#     'exterior-caulking-photos', views.ExteriorCaulkingPhotosViewSet)
# router.register(
#     'exterior-fascia-photos', views.ExteriorFasciaPhotosViewSet)
# router.register(
#     'exterior-soffit-photos', views.ExteriorSoffitPhotosViewSet)
# router.register(
#     'exterior-windows-photos', views.ExteriorWindowsPhotosViewSet)
# router.register(
#     'exterior-slab-on-grade-photos', views.ExteriorSlabOnGradePhotosViewSet)
# router.register(
#     'exterior-service-entry-photos', views.ExteriorElectricalPhotosViewSet)
# router.register(
#     'exterior-doors-photos', views.ExteriorDoorsPhotosViewSet)
# router.register(
#     'roof-general-photos', views.RoofGeneralPhotosViewSet)
# router.register(
#     'roof-style-photos', views.RoofStylePhotosViewSet)
# router.register(
#     'roof-ventilation-photos', views.RoofVentilationPhotosViewSet)
# router.register(
#     'roof-flashing-photos', views.RoofFlashingPhotosViewSet)
# router.register(
#     'roof-valleys-photos', views.RoofValleysPhotosViewSet)
# router.register(
#     'roof-condition-photos', views.RoofConditionPhotosViewSet)
# router.register(
#     'roof-skylights-photos', views.RoofSkylightsPhotosViewSet)
# router.register(
#     'roof-plumbing-vents-photos',
#     views.RoofPlumbingVentsPhotosViewSet)
# router.register(
#     'garage-type-photos', views.GarageTypePhotosViewSet)
# router.register(
#     'garage-automatic-opener-photos',
#     views.GarageAutomaticOpenerPhotosViewSet)
# router.register(
#     'garage-safety-reverse-photos',
#     views.GarageSafetyReversePhotosViewSet)
# router.register(
#     'garage-roofing-photos', views.GarageRoofingPhotosViewSet)
# router.register(
#     'garage-gutters-photos', views.GarageGuttersPhotosViewSet)
# router.register(
#     'garage-siding-photos', views.GarageSidingPhotosViewSet)
# router.register(
#     'garage-trim-photos', views.GarageTrimPhotosViewSet)
# router.register(
#     'garage-floor-photos', views.GarageFloorPhotosViewSet)
# router.register(
#     'garage-sill-plates-photos', views.GarageSillPlatesPhotosViewSet)
# router.register(
#     'garage-overhead-door-photos', views.GarageOverheadDoorPhotosViewSet)
# router.register(
#     'garage-exterior-door-photos', views.GarageExteriorDoorPhotosViewSet)
# router.register(
#     'garage-electrical-photos', views.GarageElectricalPhotosViewSet)
# router.register(
#     'garage-fire-seperation-photos',
#     views.GarageFireSeperationPhotosViewSet)
# router.register(
#     'kitchen-cabinets-photos', views.KitchenCabinetsPhotosViewSet)
# router.register(
#     'kitchen-countertops-photos', views.KitchenCountertopPhotosViewSet)
# router.register(
#     'kitchen-plumbing-photos', views.KitchenPlumbingPhotosViewSet)
# router.register(
#     'kitchen-floor-photos', views.KitchenFloorPhotosViewSet)
# router.register(
#     'kitchen-walls-ceiling-photos',
#     views.KitchenWallsCeilingPhotosViewSet)
# router.register(
#     'kitchen-appliances-photos', views.KitchenAppliancesPhotosViewSet)
# router.register(
#     'basement-stairs-photos', views.BasementStairsPhotosViewSet)
# router.register(
#     'basement-foundation-photos',
#     views.BasementFoundationPhotosViewSet)
# router.register(
#     'basement-floor-photos', views.BasementFloorPhotosViewSet)
# router.register(
#     'basement-drainage-photos', views.BasementDrainagePhotosViewSet)
# router.register(
#     'basement-seismic-bolts-photos',
#     views.BasementSeismicBoltsPhotosViewSet)
# router.register(
#     'basement-girders-beams-photos',
#     views.BasementGirdersBeamsPhotosViewSet)
# router.register(
#     'basement-columns-photos', views.BasementColumnsPhotosViewSet)
# router.register(
#     'basement-joists-photos', views.BasementJoistsPhotosViewSet)
# router.register(
#     'basement-subfloor-photos', views.BasementSubfloorPhotosViewSet)
# router.register(
#     'crawlspace-crawl-photos', views.CrawlspaceCrawlPhotosViewSet)
# router.register(
#     'crawlspace-access-photos', views.CrawlspaceAccessPhotosViewSet)
# router.register(
#     'crawlspace-foundation-photos', views.CrawlspaceFoundationPhotosViewSet)
# router.register(
#     'crawlspace-floor-photos', views.CrawlspaceFloorPhotosViewSet)
# router.register(
#     'crawlspace-seismic-bolts-photos',
#     views.CrawlspaceSeismicBoltsPhotosViewSet)
# router.register(
#     'crawlspace-joists-photos', views.CrawlspaceJoistsPhotosViewSet)
# router.register(
#     'crawlspace-girders-beams-photos',
#     views.CrawlspaceGirdersBeamsPhotosViewSet)
# router.register(
#     'crawlspace-ventilation-photos', views.CrawlspaceVentilationPhotosViewSet)
# router.register(
#     'crawlspace-insulation-photos', views.CrawlspaceInsulationPhotosViewSet)
# router.register(
#     'crawlspace-subfloor-photos', views.CrawlspaceSubfloorPhotosViewSet)
# router.register(
#     'crawlspace-access-photos', views.CrawlspaceAccessPhotosViewSet)
# router.register(
#     'crawlspace-vapor-barrier-photos',
#     views.CrawlspaceVaporBarrierPhotosViewSet)
# router.register(
#     'plumbing-water-service-photos', views.PlumbingWaterServicePhotosViewSet)
# router.register(
#     'plumbing-gas-shutoff-photos', views.PlumbingGasShutoffPhotosViewSet)
# router.register(
#     'plumbing-water-heater-photos', views.PlumbingWaterHeaterPhotosViewSet)
# router.register(
#     'plumbing-well-pump-photos', views.PlumbingWellPumpPhotosViewSet)
# router.register(
#     'plumbing-sanitary-pump-photos', views.PlumbingSanitaryPumpPhotosViewSet)
# router.register(
#     'electrical-main-panel-photos', views.ElectricalMainPanelPhotosViewSet)
# router.register(
#     'electrical-sub-panel-photos', views.ElectricalSubPanelPhotosViewSet)
# router.register(
#     'heating-furnace-photos', views.HeatingFurnacePhotosViewSet)
# router.register(
#     'heating-boiler-photos', views.HeatingBoilerPhotosViewSet)
# router.register(
#     'heating-other-systems-photos', views.HeatingOtherSystemsPhotosViewSet)
# router.register(
#     'exterior-cooling-unit-photos', views.ExteriorCoolingUnitPhotosViewSet)
# router.register(
#     'heating-evap-coil-photos', views.HeatingEvaporatorCoilPhotosViewSet)
# router.register(
#     'interior-fireplace-photos', views.InteriorFireplacePhotosViewSet)
# router.register(
#     'interior-steps-photos', views.InteriorStepsPhotosViewSet)
# router.register(
#     'interior-smoke-carbon-det-photos',
#     views.InteriorSmokeCarbonDetPhotosViewSet)
# router.register(
#     'living-room-photos', views.LivingRoomPhotosViewSet)
# router.register(
#     'dining-room-photos', views.DiningRoomPhotosViewSet)
# router.register(
#     'bedroom-photos', views.BedroomPhotosViewSet)
# router.register(
#     'dining-room-photos', views.DiningRoomPhotosViewSet)
# router.register(
#     'bathroom-electrical-photos', views.BathroomElectricalPhotosViewSet)
# router.register(
#     'bathroom-plumbing-photos', views.BathroomPlumbingPhotosViewSet)


# fiverr link to generate reports
# router.register(
#     'generate-report', views.GenerateReport.as_view())

app_name = 'report'

urlpatterns = [
    path('generate-report-html', views.GenerateReportHTML.as_view(), name='generate-report-html'),
    path('', include(router.urls)),
]
