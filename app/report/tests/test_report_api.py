# # """
# # Tests for report API.
# # """

# from django.contrib.auth import get_user_model
# from django.test import TestCase
# from django.urls import reverse
# from rest_framework.authtoken.models import Token
# from rest_framework import status
# from rest_framework.test import APIClient

# from core.models import ReportDetails

# from report.serializers import ReportDetailsSerializer

# REPORT_URL = reverse('report:report-list')


# # def detail_url(report_uuid):
# #     """Create and return a recipe detail URL."""
# #     return reverse('report-details', args=[report_uuid])

# def create_report(user, **params):
#     """Create and return a sample recipe."""
#     defaults = {
#         'title': 'Sample report title',
#         'r_id': 'Sample R_id',
#         'date': '2023-09-18',
#         'client_name': 'test client name',
#         'bedroom_count': 3,
#         'bathroom_count': 3,
#         'garage_type': 'Detached',
#         'basement_type': 'slab',
#     }
#     defaults.update(params)

#     report = ReportDetails.objects.create(user=user, **defaults)
#     return report


# def create_user(**params):
#     """Create and return a new user."""
#     return get_user_model().objects.create_user(**params)


# class PublicReportAPITests(TestCase):
#     """Test unauthenticated API requests."""

#     def setUp(self):
#         self.client = APIClient()
#         self.user = create_user(email='user@example.com',
# password='testpass123')

#     def test_auth_required(self):
#         """Test auth is required to call API."""
#         res = self.client.get(REPORT_URL)

#         print("Response Status Code:", res.status_code)

#         self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)


# class PrivateReportApiTests(TestCase):
#     """Test authenticated API requests."""

#     def setUp(self):
#         self.client = APIClient()
#         self.user = get_user_model().objects.create_user(
#             email='user@example.com',
#             password='testpass123',
#         )
#         self.client.force_authenticate(user=self.user)

#     def test_auth_required(self):
#         """Test auth is required to call API."""
#         res = self.client.get(REPORT_URL)

#         self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)


#     def test_retrieve_report(self):
#         """Test retrieving a list of reports."""
#         create_report(user=self.user)

#         res = self.client.get(REPORT_URL)
#         report = ReportDetails.objects.all().order_by('-report_uuid')
#         serializer = ReportDetailsSerializer(report, many=True)

#         self.assertEqual(res.status_code, status.HTTP_200_OK)
#         self.assertEqual(res.data, serializer.data)


#     def test_recipe_list_limited_to_user(self):
#         """Test list of reports is limited to company users."""
#         other_user = get_user_model().objects.create_user(
#             'other@example.com',
#             'password123',
#         )
#         create_report(user=other_user)

#         create_report(user=self.user)

#         res = self.client.get(REPORT_URL)
#         actual_data = sorted(res.data, key=lambda x: x['report_uuid'])
#         expected_reports = ReportDetails.objects.filter(user=self.user)
#         serializer = ReportDetailsSerializer(
# expected_reports, many=True)
#         expected_data = sorted(
# serializer.data, key=lambda x: x['report_uuid'])

#         self.assertEqual(res.status_code, status.HTTP_200_OK)
#         self.assertEqual(actual_data, expected_data)
