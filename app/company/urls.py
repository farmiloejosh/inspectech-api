from django.urls import (
    path,
    include,
)

from rest_framework.routers import DefaultRouter
from company import views

router = DefaultRouter()
router.register(
    'company-details', views.CompanyDetailsViewSet)
router.register(
    'company-logo', views.CompanyLogoViewSet)
router.register(
    'company-signature', views.CompanySignatureViewSet)

app_name = 'company'

urlpatterns = [
    path('', include(router.urls)),
]
