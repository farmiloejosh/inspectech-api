terraform {
  backend "s3" {
    bucket         = "inspectech-api-tfstate"
    key            = "inspectech-api.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "inspectech-api-tfstate-lock"
  }
}

provider "aws" {
  region = "us-east-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}
data "aws_region" "current" {}
