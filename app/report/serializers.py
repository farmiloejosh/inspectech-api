"""
Serializers for reports API.
"""
from rest_framework import serializers


from core.models import (
    ReportDetails, Overview, Summary,
    ReceiptInvoice, Grounds, Roof,
    GarageCarport, Exterior,
    Kitchen, Laundry, Basement, Crawlspace,
    Bathroom, Bedrooms, Interior, Plumbing,
    ElectricalCoolingSystems, PlumbingSanitaryPumpPhotos,
    PlumbingWellPumpPhotos,
    HeatingSystem, DiningRoom, LivingRoom, BathroomElectricalPhotos,
    BathroomPlumbingPhotos,
    ExteriorCaulkingPhotos, ExteriorChimneyPhotos, ExteriorDoorsPhotos,
    ExteriorElectricalPhotos, ExteriorFasciaPhotos, ExteriorFlashingPhotos,
    ExteriorGuttersPhotos, ExteriorSidingPhotos, ExteriorSoffitPhotos,
    ExteriorTrimPhotos, ExteriorWindowsPhotos, GroundsCoversPhotos,
    GarageAutomaticOpenerPhotos, GroundsDeckPhotos, GroundsDrivewayPhotos,
    GroundsFencePhotos, GroundsHoseBibsPhotos, GroundsLandscapingPhotos,
    GroundsPatioPhotos, GroundsPorchPhotos, GroundsRetainingWallPhotos,
    GroundsSideWalksPhotos, GroundsStoopStepsPhotos, RoofConditionPhotos,
    RoofFlashingPhotos, RoofGeneralPhotos, RoofPlumbingVentsPhotos,
    RoofSkylightsPhotos, RoofStylePhotos, RoofValleysPhotos,
    RoofVentilationPhotos, GarageElectricalPhotos, GarageExteriorDoorPhotos,
    GarageFireSeperationPhotos, GarageFloorPhotos, GarageGuttersPhotos,
    GarageOverheadDoorPhotos, GarageRoofingPhotos, GarageSafetyReversePhotos,
    GarageSidingPhotos, GarageTrimPhotos, GarageTypePhotos,
    BasementColumnsPhotos, BasementDrainagePhotos, BasementFloorPhotos,
    BasementFoundationPhotos, BasementJoistsPhotos, BasementStairsPhotos,
    BasementSubfloorPhotos, DiningRoomPhotos, InteriorAtticPhotos,
    InteriorFireplacePhotos, InteriorSmokeCarbonDetPhotos,
    InteriorStepsPhotos,
    BedroomPhotos, BoilerPhotos, FurnacePhotos, HeatingOtherSystemsPhotos,
    LivingRoomPhotos, KitchenAppliancesPhotos, KitchenCabinetsPhotos,
    KitchenCountertopPhotos, KitchenFloorPhotos, KitchenPlumbingPhotos,
    KitchenWallsCeilingPhotos, BasementGirdersBeamsPhotos,
    PlumbingWaterServicePhotos, PlumbingWaterHeaterPhotos,
    PlumbingGasShutoffPhotos,
    BasementSeismicBoltsPhotos, GarageSillPlatesPhotos,
    ExteriorCoolingUnitPhotos, ReportDetailsPhoto,
    ElectricalMainPanelPhotos, ElectricalSubPanelPhotos, LaundryPhotos,
    RoofGeneralSummary, RoofConditionSummary, RoofFlashingSummary,
    RoofPlumbingVentsSummary, RoofSkylightsSummary, RoofStyleSummary,
    RoofValleysSummary, RoofVentilationSummary, ExteriorCaulkingSummary,
    ExteriorChimneySummary, ExteriorDoorsSummary, ExteriorFasciaSummary,
    ExteriorFlashingSummary, ExteriorGuttersSummary,
    ExteriorSidingSummary,
    ExteriorSlabOnFoundationSummary, ExteriorSoffitSummary,
    ExteriorTrimSummary,
    ExteriorWindowsSummary, GroundsCoversSummary, GroundsDeckSummary,
    GroundsDrivewaySummary, GroundsFenceSummary, GroundsHoseBibsSummary,
    GroundsLandscapingSummary, GroundsPatioSummary,
    GroundsRetainingWallSummary,
    GarageAutomaticOpenerSummary, GroundsSidewalkSummary,
    GroundsStoopStepsSummary,
    GarageElectricalSummary, GarageExteriorDoorSummary,
    GarageFireSeperationSummary,
    GarageFloorSummary, GarageGuttersSummary, GarageOverheadDoorSummary,
    GarageRoofingSummary, GarageSafetyReverseSummary,
    GarageSidingSummary, ExteriorSlabOnGradePhotos,
    GarageTypeSummary, BasementColumnsSummary, BasementDrainageSummary,
    BasementFloorSummary, BasementFoundationSummary,
    BasementGirdersBeamsSummary,
    BasementJoistsSummary, BasementSeismicBoltsSummary,
    BasementSubfloorSummary,
    BathroomElectricalSummary, BathroomPlumbingSummary,
    KitchenAppliancesSummary,
    KitchenCabinetsSummary, KitchenCountertopSummary,
    KitchenFloorSummary,
    KitchenPlumbingSummary, KitchenWallsCeilingSummary,
    InteriorFireplaceSummary, PlumbingGasShutoffSummary,
    PlumbingWaterHeaterSummary, PlumbingWaterServiceSummary,
    GarageSillPlatesSummary, InteriorStepsSummary,
    InteriorSmokeCarbonDetSummary, InteriorAtticSummary,
    BasementStairsSummary, ExteriorWallConstructionSummary,
    BedroomSummary, GroundsPorchSummary, ElectricalMainPanelSummary,
    ElectricalSubPanelSummary, CrawlspaceCrawlSummary,
    CrawlspaceAccessSummary, ExteriorServiceEntrySummary,
    CrawlspaceInsulationSummary, CrawlspaceVaporBarrierSummary,
    CrawlspaceVentilationSummary, CrawlspaceCrawlPhotos,
    CrawlspaceAccessPhotos, CrawlspaceDrainagePhotos,
    CrawlspaceFloorPhotos, CrawlspaceFoundationPhotos,
    CrawlspaceGirdersBeamsPhotos, CrawlspaceInsulationPhotos,
    CrawlspaceJoistsPhotos, CrawlspaceSubfloorPhotos,
    CrawlspaceVaporBarrierPhotos, CrawlspaceVentilationPhotos,
    CrawlspaceSeismicBoltsPhotos, EvaporatorCoilPhotos,
    PhotoAndComments, GenerateReport, UserCommentsSummary
)


class ReportDetailsSerializer(serializers.ModelSerializer):
    """Report Detail Serializer"""
    class Meta:
        model = ReportDetails
        fields = ['report_uuid', 'title', 'r_id',
                  'date', 'client_name', 'address',
                  'bedroom_count', 'bathroom_count', 'garage_type',
                  'basement_type']
        read_only_fields = ['user']


class ReportDetailsPhotoSerializer(serializers.ModelSerializer):
    """Serializer for uploading images to recipes."""

    class Meta:
        model = ReportDetailsPhoto
        fields = ['report_uuid', 'report_details_photo']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class OverviewSerializer(serializers.ModelSerializer):
    """Serializer for overview model"""

    class Meta:
        model = Overview
        fields = ['report_uuid', 'scope', 'state_of_occupancy',
                  'weather', 'recent_rain', 'ground_cover',
                  'approx_age']
        read_only_fields = ['user']


class SummarySerializer(serializers.ModelSerializer):
    """Serializer for summary model"""

    class Meta:
        model = Summary
        fields = ['report_uuid', 'major_concerns', 'safety_hazards',
                  'further_review', 'monitor',
                  'general_maintenance', 'needing_repair',
                  'for_your_info']
        read_only_fields = ['user']


class ExteriorChimneyPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to exterior."""

    class Meta:
        model = ExteriorChimneyPhotos
        fields = ['report_uuid', 'exterior_chimney_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class ExteriorGuttersPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to exterior."""

    class Meta:
        model = ExteriorGuttersPhotos
        fields = ['report_uuid', 'exterior_gutters_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class ExteriorSidingPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to exterior."""

    class Meta:
        model = ExteriorSidingPhotos
        fields = ['report_uuid', 'exterior_siding_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class ExteriorTrimPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to exterior."""

    class Meta:
        model = ExteriorTrimPhotos
        fields = ['report_uuid', 'exterior_trim_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class ExteriorSoffitPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to exterior."""

    class Meta:
        model = ExteriorSoffitPhotos
        fields = ['report_uuid', 'exterior_soffit_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class ExteriorFasciaPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to exterior."""

    class Meta:
        model = ExteriorFasciaPhotos
        fields = ['report_uuid', 'exterior_fascia_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class ExteriorFlashingPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to exterior."""

    class Meta:
        model = ExteriorFlashingPhotos
        fields = ['report_uuid', 'exterior_flashing_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class ExteriorCaulkingPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to exterior."""

    class Meta:
        model = ExteriorCaulkingPhotos
        fields = ['report_uuid', 'exterior_caulking_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class ExteriorWindowsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to exterior."""

    class Meta:
        model = ExteriorWindowsPhotos
        fields = ['report_uuid', 'exterior_windows_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class ExteriorElectricalPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to exterior."""

    class Meta:
        model = ExteriorElectricalPhotos
        fields = ['report_uuid', 'exterior_electrical_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class ExteriorDoorsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to exterior."""

    class Meta:
        model = ExteriorDoorsPhotos
        fields = ['report_uuid', 'exterior_doors_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class ExteriorSlabOnGradePhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to exterior."""

    class Meta:
        model = ExteriorSlabOnGradePhotos
        fields = ['report_uuid', 'exterior_slab_on_grade_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class PhotoAndCommentsSerializer(serializers.ModelSerializer):
    """Dining Room Serializer"""

    class Meta:
        model = PhotoAndComments
        fields = '__all__'
        # read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}

class GenerateReportSerializer(serializers.ModelSerializer):
    """Dining Room Serializer"""

    class Meta:
        model = GenerateReport
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GroundsDrivewayPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to grounds."""

    class Meta:
        model = GroundsDrivewayPhotos
        fields = ['report_uuid', 'grounds_driveway_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GroundsSidewalkPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to grounds."""

    class Meta:
        model = GroundsSideWalksPhotos
        fields = ['report_uuid', 'grounds_sidewalk_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GroundsStoopStepsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to grounds."""

    class Meta:
        model = GroundsStoopStepsPhotos
        fields = ['report_uuid', 'grounds_stoop_steps_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GroundsPorchPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to grounds."""

    class Meta:
        model = GroundsPorchPhotos
        fields = ['report_uuid', 'grounds_porch_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GroundsPatioPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to grounds."""

    class Meta:
        model = GroundsPatioPhotos
        fields = ['report_uuid', 'grounds_patio_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GroundsDeckPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to grounds."""

    class Meta:
        model = GroundsDeckPhotos
        fields = ['report_uuid', 'grounds_deck_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GroundsFencePhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to grounds."""

    class Meta:
        model = GroundsFencePhotos
        fields = ['report_uuid', 'grounds_fence_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GroundsHoseBibsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to grounds."""

    class Meta:
        model = GroundsHoseBibsPhotos
        fields = ['report_uuid', 'grounds_hose_bibs_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GroundsLandscapingPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to grounds."""

    class Meta:
        model = GroundsLandscapingPhotos
        fields = ['report_uuid', 'grounds_landscaping_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GroundsRetainingWallPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to grounds."""

    class Meta:
        model = GroundsRetainingWallPhotos
        fields = ['report_uuid', 'grounds_retaining_wall_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GroundsCoversPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to grounds."""

    class Meta:
        model = GroundsCoversPhotos
        fields = ['report_uuid', 'grounds_covers_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class RoofGeneralPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to roof."""

    class Meta:
        model = RoofGeneralPhotos
        fields = ['report_uuid', 'roof_general_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class RoofStylePhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to roof."""

    class Meta:
        model = RoofStylePhotos
        fields = ['report_uuid', 'roof_style_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class RoofVentilationPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to roof."""

    class Meta:
        model = RoofVentilationPhotos
        fields = ['report_uuid', 'roof_ventilation_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class RoofFlashingPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to roof."""

    class Meta:
        model = RoofFlashingPhotos
        fields = ['report_uuid', 'roof_flashing_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class RoofValleysPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to roof."""

    class Meta:
        model = RoofValleysPhotos
        fields = ['report_uuid', 'roof_valleys_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class RoofConditionPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to roof."""

    class Meta:
        model = RoofConditionPhotos
        fields = ['report_uuid', 'roof_condition_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class RoofSkylightsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to roof."""

    class Meta:
        model = RoofSkylightsPhotos
        fields = ['report_uuid', 'roof_skylights_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class RoofPlumbingVentsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to roof."""

    class Meta:
        model = RoofPlumbingVentsPhotos
        fields = ['report_uuid', 'roof_plumbing_vents_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GarageTypePhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to garage."""

    class Meta:
        model = GarageTypePhotos
        fields = ['report_uuid', 'garage_type_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GarageAutomaticOpenerPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to garage."""

    class Meta:
        model = GarageAutomaticOpenerPhotos
        fields = ['report_uuid', 'garage_automatic_opener_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GarageSafetyReversePhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to garage."""

    class Meta:
        model = GarageSafetyReversePhotos
        fields = ['report_uuid', 'garage_safety_reverse_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GarageRoofingPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to garage."""

    class Meta:
        model = GarageRoofingPhotos
        fields = ['report_uuid', 'garage_roofing_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GarageGuttersPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to garage."""

    class Meta:
        model = GarageGuttersPhotos
        fields = ['report_uuid', 'garage_gutters_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GarageFloorPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to garage."""

    class Meta:
        model = GarageFloorPhotos
        fields = ['report_uuid', 'garage_floor_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GarageSillPlatesPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to garage."""

    class Meta:
        model = GarageSillPlatesPhotos
        fields = ['report_uuid', 'garage_sill_plates_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GarageSidingPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to garage."""

    class Meta:
        model = GarageSidingPhotos
        fields = ['report_uuid', 'garage_siding_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GarageTrimPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to garage."""

    class Meta:
        model = GarageTrimPhotos
        fields = ['report_uuid', 'garage_trim_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GarageOverheadDoorPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to garage."""

    class Meta:
        model = GarageOverheadDoorPhotos
        fields = ['report_uuid', 'garage_overhead_door_photos']
        read_only_fields = ['report_uuid']


class GarageExteriorDoorPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to garage."""

    class Meta:
        model = GarageExteriorDoorPhotos
        fields = ['report_uuid', 'garage_exterior_door_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GarageElectricalPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to garage."""

    class Meta:
        model = GarageElectricalPhotos
        fields = ['report_uuid', 'garage_electrical_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GarageFireSeperationPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to garage."""

    class Meta:
        model = GarageFireSeperationPhotos
        fields = ['report_uuid', 'garage_fire_seperation_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class KitchenCountertopPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to kitchen."""

    class Meta:
        model = KitchenCountertopPhotos
        fields = ['report_uuid', 'kitchen_countertop_photos']
        read_only_fields = ['report_uuid']


class KitchenCabinetsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to kitchen."""

    class Meta:
        model = KitchenCabinetsPhotos
        fields = ['report_uuid', 'kitchen_cabinets_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class KitchenPlumbingPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to kitchen."""

    class Meta:
        model = KitchenPlumbingPhotos
        fields = ['report_uuid', 'kitchen_plumbing_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class KitchenFloorPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to kitchen."""

    class Meta:
        model = KitchenFloorPhotos
        fields = ['report_uuid', 'kitchen_floor_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class KitchenWallsCeilingPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to kitchen."""

    class Meta:
        model = KitchenWallsCeilingPhotos
        fields = ['report_uuid', 'kitchen_walls_ceiling_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class KitchenAppliancesPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to kitchen."""

    class Meta:
        model = KitchenAppliancesPhotos
        fields = ['report_uuid', 'kitchen_appliances_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class LaundryPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to laundry."""

    class Meta:
        model = LaundryPhotos
        fields = ['report_uuid', 'laundry_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class BathroomElectricalPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to exterior."""

    class Meta:
        model = BathroomElectricalPhotos
        fields = ['report_uuid', 'bathroom_electrical_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class BathroomPlumbingPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to exterior."""

    class Meta:
        model = BathroomPlumbingPhotos
        fields = ['report_uuid', 'bathroom_plumbing_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class BasementStairsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to basement."""

    class Meta:
        model = BasementStairsPhotos
        fields = ['report_uuid', 'basement_stairs_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class BasementFoundationPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to basement."""

    class Meta:
        model = BasementFoundationPhotos
        fields = ['report_uuid', 'basement_foundation_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class BasementColumnsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to basement."""

    class Meta:
        model = BasementColumnsPhotos
        fields = ['report_uuid', 'basement_columns_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class BasementGirdersBeamsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to basement."""

    class Meta:
        model = BasementGirdersBeamsPhotos
        fields = ['report_uuid', 'basement_girders_beams_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class BasementFloorPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to basement."""

    class Meta:
        model = BasementFloorPhotos
        fields = ['report_uuid', 'basement_floor_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class BasementSeismicBoltsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to basement."""

    class Meta:
        model = BasementSeismicBoltsPhotos
        fields = ['report_uuid', 'basement_seismic_bolts_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class BasementDrainagePhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to basement."""

    class Meta:
        model = BasementDrainagePhotos
        fields = ['report_uuid', 'basement_drainage_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class BasementJoistsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to basement."""

    class Meta:
        model = BasementJoistsPhotos
        fields = ['report_uuid', 'basement_joists_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class BasementSubfloorPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to basement."""

    class Meta:
        model = BasementSubfloorPhotos
        fields = ['report_uuid', 'basement_subfloor_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class CrawlspaceCrawlPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to crawl."""

    class Meta:
        model = CrawlspaceCrawlPhotos
        fields = ['report_uuid', 'crawlspace_crawl_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class CrawlspaceAccessPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to crawl."""

    class Meta:
        model = CrawlspaceAccessPhotos
        fields = ['report_uuid', 'crawlspace_access_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class CrawlspaceFoundationPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to crawl."""

    class Meta:
        model = CrawlspaceFoundationPhotos
        fields = ['report_uuid', 'crawlspace_foundation_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class CrawlspaceFloorPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to crawl."""

    class Meta:
        model = CrawlspaceFloorPhotos
        fields = ['report_uuid', 'crawlspace_floor_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class CrawlspaceDrainagePhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to crawl."""

    class Meta:
        model = CrawlspaceDrainagePhotos
        fields = ['report_uuid', 'crawlspace_drainage_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class CrawlspaceSeismicBoltsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to crawl."""

    class Meta:
        model = CrawlspaceSeismicBoltsPhotos
        fields = ['report_uuid', 'crawlspace_seismic_bolts_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class CrawlspaceVentilationPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to crawl."""

    class Meta:
        model = CrawlspaceVentilationPhotos
        fields = ['report_uuid', 'crawlspace_ventilation_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class CrawlspaceGirdersBeamsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to crawl."""

    class Meta:
        model = CrawlspaceGirdersBeamsPhotos
        fields = ['report_uuid', 'crawlspace_girders_beams_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class CrawlspaceJoistsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to crawl."""

    class Meta:
        model = CrawlspaceJoistsPhotos
        fields = ['report_uuid', 'crawlspace_joists_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class CrawlspaceSubfloorPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to crawl."""

    class Meta:
        model = CrawlspaceSubfloorPhotos
        fields = ['report_uuid', 'crawlspace_subfloor_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class CrawlspaceInsulationPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to crawl."""

    class Meta:
        model = CrawlspaceInsulationPhotos
        fields = ['report_uuid', 'crawlspace_insulation_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class CrawlspaceVaporBarrierPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to crawl."""

    class Meta:
        model = CrawlspaceVaporBarrierPhotos
        fields = ['report_uuid', 'crawlspace_vapor_barrier_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class PlumbingWaterServicePhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to plumbing."""

    class Meta:
        model = PlumbingWaterServicePhotos
        fields = ['report_uuid', 'plumbing_water_service_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class PlumbingGasShutoffPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to plumbing."""

    class Meta:
        model = PlumbingGasShutoffPhotos
        fields = ['report_uuid', 'plumbing_gas_shutoff_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class PlumbingWaterHeaterPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to plumbing."""

    class Meta:
        model = PlumbingWaterHeaterPhotos
        fields = ['report_uuid', 'plumbing_water_heater_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class PlumbingWellPumpPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to plumbing."""

    class Meta:
        model = PlumbingWellPumpPhotos
        fields = ['report_uuid', 'plumbing_well_pump_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class PlumbingSanitaryPumpPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to plumbing."""

    class Meta:
        model = PlumbingSanitaryPumpPhotos
        fields = ['report_uuid', 'plumbing_sanitary_pump_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class ElectricalMainPanelPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to plumbing."""

    class Meta:
        model = ElectricalMainPanelPhotos
        fields = ['report_uuid', 'electrical_main_panel_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class ElectricalSubPanelPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to plumbing."""

    class Meta:
        model = ElectricalSubPanelPhotos
        fields = ['report_uuid', 'electrical_sub_panel_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class FurnacePhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to heating."""

    class Meta:
        model = FurnacePhotos
        fields = ['report_uuid', 'furnace_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class BoilerPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to heating."""

    class Meta:
        model = BoilerPhotos
        fields = ['report_uuid', 'boiler_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class HeatingOtherSystemsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to heating."""

    class Meta:
        model = HeatingOtherSystemsPhotos
        fields = ['report_uuid', 'heating_other_systems_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class ExteriorCoolingUnitPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to exterior."""

    class Meta:
        model = ExteriorCoolingUnitPhotos
        fields = ['report_uuid', 'exterior_cooling_unit_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class EvaporatorCoilPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to heating."""

    class Meta:
        model = EvaporatorCoilPhotos
        fields = ['report_uuid', 'evaporator_coil_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class LivingRoomPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to living room."""

    class Meta:
        model = LivingRoomPhotos
        fields = ['report_uuid', 'living_room_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class BedroomPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to bedroom."""

    class Meta:
        model = BedroomPhotos
        fields = ['report_uuid', 'bedroom_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class InteriorFireplacePhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to interior."""

    class Meta:
        model = InteriorFireplacePhotos
        fields = ['report_uuid', 'interior_fireplace_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class InteriorStepsPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to interior."""

    class Meta:
        model = InteriorStepsPhotos
        fields = ['report_uuid', 'interior_steps_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class InteriorSmokeCarbonDetPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to interior."""

    class Meta:
        model = InteriorSmokeCarbonDetPhotos
        fields = ['report_uuid', 'interior_smoke_carbon_det_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class InteriorAtticPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to Attic."""

    class Meta:
        model = InteriorAtticPhotos
        fields = ['report_uuid', 'interior_attic_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class DiningRoomPhotosSerializer(serializers.ModelSerializer):
    """Serializer for uploading photos to dining room."""

    class Meta:
        model = DiningRoomPhotos
        fields = ['report_uuid', 'dining_room_photos']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class RoofGeneralSummarySerializer(serializers.ModelSerializer):
    """Serializer for roof summary."""

    class Meta:
        model = RoofGeneralSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class RoofStyleSummarySerializer(serializers.ModelSerializer):
    """Serializer for roof."""

    class Meta:
        model = RoofStyleSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class RoofVentilationSummarySerializer(serializers.ModelSerializer):
    """Serializer for roof summary."""

    class Meta:
        model = RoofVentilationSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class RoofFlashingSummarySerializer(serializers.ModelSerializer):
    """Serializer for roof summary."""

    class Meta:
        model = RoofFlashingSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class RoofValleysSummarySerializer(serializers.ModelSerializer):
    """Serializer for roof summary."""

    class Meta:
        model = RoofValleysSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class RoofConditionSummarySerializer(serializers.ModelSerializer):
    """Serializer for roof summary."""

    class Meta:
        model = RoofConditionSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class RoofSkylightsSummarySerializer(serializers.ModelSerializer):
    """Serializer for roof summary."""

    class Meta:
        model = RoofSkylightsSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class RoofPlumbingVentsSummarySerializer(serializers.ModelSerializer):
    """Serializer for roof summary."""

    class Meta:
        model = RoofPlumbingVentsSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class ExteriorChimneySummarySerializer(serializers.ModelSerializer):
    """Serializer for exterior summary."""

    class Meta:
        model = ExteriorChimneySummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class ExteriorSidingSummarySerializer(serializers.ModelSerializer):
    """Serializer for exterior summary."""

    class Meta:
        model = ExteriorSidingSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class ExteriorTrimSummarySerializer(serializers.ModelSerializer):
    """Serializer for exterior summary."""

    class Meta:
        model = ExteriorTrimSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class ExteriorFlashingSummarySerializer(serializers.ModelSerializer):
    """Serializer for exterior summary."""

    class Meta:
        model = ExteriorFlashingSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class ExteriorGuttersSummarySerializer(serializers.ModelSerializer):
    """Serializer for exterior summary."""

    class Meta:
        model = ExteriorGuttersSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class ExteriorCaulkingSummarySerializer(serializers.ModelSerializer):
    """Serializer for exterior summary."""

    class Meta:
        model = ExteriorCaulkingSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class ExteriorSoffitSummarySerializer(serializers.ModelSerializer):
    """Serializer for exterior summary."""

    class Meta:
        model = ExteriorSoffitSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class ExteriorWindowsSummarySerializer(serializers.ModelSerializer):
    """Serializer for exterior summary."""

    class Meta:
        model = ExteriorWindowsSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class ExteriorSlabOnFoundationSummarySerializer(serializers.ModelSerializer):
    """Serializer for exterior summary."""

    class Meta:
        model = ExteriorSlabOnFoundationSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class ExteriorFasciaSummarySerializer(serializers.ModelSerializer):
    """Serializer for exterior summary."""

    class Meta:
        model = ExteriorFasciaSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class ExteriorServiceEntrySummarySerializer(serializers.ModelSerializer):
    """Serializer for exterior summary."""

    class Meta:
        model = ExteriorServiceEntrySummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class ExteriorWallConstructionSummarySerializer(serializers.ModelSerializer):
    """Serializer for exterior summary."""

    class Meta:
        model = ExteriorWallConstructionSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class ExteriorDoorsSummarySerializer(serializers.ModelSerializer):
    """Serializer for exterior summary."""

    class Meta:
        model = ExteriorDoorsSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']



class UserCommentsSummarySerializer(serializers.ModelSerializer):
    """Serializer for all user comments summary."""

    class Meta:
        model = UserCommentsSummary
        fields = ['id', 'type', 'title', 'comment']
        read_only_field = ['id']



class GroundsDrivewaySummarySerializer(serializers.ModelSerializer):
    """Serializer for grounds summary."""

    class Meta:
        model = GroundsDrivewaySummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GroundsSidewalkSummarySerializer(serializers.ModelSerializer):
    """Serializer for grounds summary."""

    class Meta:
        model = GroundsSidewalkSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GroundsStoopStepsSummarySerializer(serializers.ModelSerializer):
    """Serializer for grounds summary."""

    class Meta:
        model = GroundsStoopStepsSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GroundsPorchSummarySerializer(serializers.ModelSerializer):
    """Serializer for grounds summary."""

    class Meta:
        model = GroundsPorchSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GroundsPatioSummarySerializer(serializers.ModelSerializer):
    """Serializer for grounds summary."""

    class Meta:
        model = GroundsPatioSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GroundsDeckSummarySerializer(serializers.ModelSerializer):
    """Serializer for grounds summary."""

    class Meta:
        model = GroundsDeckSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GroundsCoversSummarySerializer(serializers.ModelSerializer):
    """Serializer for grounds summary."""

    class Meta:
        model = GroundsCoversSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GroundsFenceSummarySerializer(serializers.ModelSerializer):
    """Serializer for grounds summary."""

    class Meta:
        model = GroundsFenceSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GroundsLandscapingSummarySerializer(serializers.ModelSerializer):
    """Serializer for grounds summary."""

    class Meta:
        model = GroundsLandscapingSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GroundsHoseBibsSummarySerializer(serializers.ModelSerializer):
    """Serializer for grounds summary."""

    class Meta:
        model = GroundsHoseBibsSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GroundsRetainingWallSummarySerializer(serializers.ModelSerializer):
    """Serializer for grounds summary."""

    class Meta:
        model = GroundsRetainingWallSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GarageTypeSummarySerializer(serializers.ModelSerializer):
    """Serializer for garage summary."""

    class Meta:
        model = GarageTypeSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GarageAutomaticOpenerSummarySerializer(serializers.ModelSerializer):
    """Serializer for garage summary."""

    class Meta:
        model = GarageAutomaticOpenerSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GarageSafetyReverseSummarySerializer(serializers.ModelSerializer):
    """Serializer for garage summary."""

    class Meta:
        model = GarageSafetyReverseSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GarageGuttersSummarySerializer(serializers.ModelSerializer):
    """Serializer for garage summary."""

    class Meta:
        model = GarageGuttersSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GarageRoofingSummarySerializer(serializers.ModelSerializer):
    """Serializer for garage summary."""

    class Meta:
        model = GarageRoofingSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GarageSidingSummarySerializer(serializers.ModelSerializer):
    """Serializer for garage summary."""

    class Meta:
        model = GarageSidingSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GarageFloorSummarySerializer(serializers.ModelSerializer):
    """Serializer for garage summary."""

    class Meta:
        model = GarageFloorSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GarageSillPlatesSummarySerializer(serializers.ModelSerializer):
    """Serializer for garage summary."""

    class Meta:
        model = GarageSillPlatesSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GarageOverheadDoorSummarySerializer(serializers.ModelSerializer):
    """Serializer for garage summary."""

    class Meta:
        model = GarageOverheadDoorSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GarageExteriorDoorSummarySerializer(serializers.ModelSerializer):
    """Serializer for garage summary."""

    class Meta:
        model = GarageExteriorDoorSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GarageElectricalSummarySerializer(serializers.ModelSerializer):
    """Serializer for garage summary."""

    class Meta:
        model = GarageElectricalSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class GarageFireSeperationSummarySerializer(serializers.ModelSerializer):
    """Serializer for garage summary."""

    class Meta:
        model = GarageFireSeperationSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class BasementStairsSummarySerializer(serializers.ModelSerializer):
    """Serializer for basement summary."""

    class Meta:
        model = BasementStairsSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class BasementFoundationSummarySerializer(serializers.ModelSerializer):
    """Serializer for basement summary."""

    class Meta:
        model = BasementFoundationSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class BasementFloorSummarySerializer(serializers.ModelSerializer):
    """Serializer for basement summary."""

    class Meta:
        model = BasementFloorSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class BasementDrainageSummarySerializer(serializers.ModelSerializer):
    """Serializer for basement summary."""

    class Meta:
        model = BasementDrainageSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class BasementSeismicBoltsSummarySerializer(serializers.ModelSerializer):
    """Serializer for basement summary."""

    class Meta:
        model = BasementSeismicBoltsSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class BasementGirdersBeamsSummarySerializer(serializers.ModelSerializer):
    """Serializer for basement summary."""

    class Meta:
        model = BasementGirdersBeamsSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class BasementColumnsSummarySerializer(serializers.ModelSerializer):
    """Serializer for basement summary."""

    class Meta:
        model = BasementColumnsSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class BasementJoistsSummarySerializer(serializers.ModelSerializer):
    """Serializer for basement summary."""

    class Meta:
        model = BasementJoistsSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class BasementSubfloorSummarySerializer(serializers.ModelSerializer):
    """Serializer for basement summary."""

    class Meta:
        model = BasementSubfloorSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class CrawlspaceCrawlSummarySerializer(serializers.ModelSerializer):
    """Serializer for crawlspace summary."""

    class Meta:
        model = CrawlspaceCrawlSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class CrawlspaceAccessSummarySerializer(serializers.ModelSerializer):
    """Serializer for crawlspace summary."""

    class Meta:
        model = CrawlspaceAccessSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class CrawlspaceVentilationSummarySerializer(serializers.ModelSerializer):
    """Serializer for crawlspace summary."""

    class Meta:
        model = CrawlspaceVentilationSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class CrawlspaceInsulationSummarySerializer(serializers.ModelSerializer):
    """Serializer for crawlspace summary."""

    class Meta:
        model = CrawlspaceInsulationSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class CrawlspaceVaporBarrierSummarySerializer(serializers.ModelSerializer):
    """Serializer for crawlspace summary."""

    class Meta:
        model = CrawlspaceVaporBarrierSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class KitchenCountertopSummarySerializer(serializers.ModelSerializer):
    """Serializer for kitchen summary."""

    class Meta:
        model = KitchenCountertopSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class KitchenCabinetsSummarySerializer(serializers.ModelSerializer):
    """Serializer for kitchen summary."""

    class Meta:
        model = KitchenCabinetsSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class KitchenPlumbingSummarySerializer(serializers.ModelSerializer):
    """Serializer for kitchen summary."""

    class Meta:
        model = KitchenPlumbingSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class KitchenFloorSummarySerializer(serializers.ModelSerializer):
    """Serializer for kitchen summary."""

    class Meta:
        model = KitchenFloorSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class KitchenWallsCeilingSummarySerializer(serializers.ModelSerializer):
    """Serializer for kitchen summary."""

    class Meta:
        model = KitchenWallsCeilingSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class KitchenAppliancesSummarySerializer(serializers.ModelSerializer):
    """Serializer for kitchen summary."""

    class Meta:
        model = KitchenAppliancesSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class PlumbingWaterServiceSummarySerializer(serializers.ModelSerializer):
    """Serializer for plumbing summary."""

    class Meta:
        model = PlumbingWaterServiceSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class PlumbingGasShutoffSummarySerializer(serializers.ModelSerializer):
    """Serializer for plumbing summary."""

    class Meta:
        model = PlumbingGasShutoffSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class PlumbingWaterHeaterSummarySerializer(serializers.ModelSerializer):
    """Serializer for plumbing summary."""

    class Meta:
        model = PlumbingWaterHeaterSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class ElectricalMainPanelSummarySerializer(serializers.ModelSerializer):
    """Serializer for main panel summary."""

    class Meta:
        model = ElectricalMainPanelSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class ElectricalSubPanelSummarySerializer(serializers.ModelSerializer):
    """Serializer for sub panel summary."""

    class Meta:
        model = ElectricalSubPanelSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class BathroomElectricalSummarySerializer(serializers.ModelSerializer):
    """Serializer for bathroom summary."""

    class Meta:
        model = BathroomElectricalSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class BathroomPlumbingSummarySerializer(serializers.ModelSerializer):
    """Serializer for bathroom summary."""

    class Meta:
        model = BathroomPlumbingSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class InteriorFireplaceSummarySerializer(serializers.ModelSerializer):
    """Serializer for interior summary."""

    class Meta:
        model = InteriorFireplaceSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class InteriorStepsSummarySerializer(serializers.ModelSerializer):
    """Serializer for interior summary."""

    class Meta:
        model = InteriorStepsSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class InteriorSmokeCarbonDetSummarySerializer(serializers.ModelSerializer):
    """Serializer for interior summary."""

    class Meta:
        model = InteriorSmokeCarbonDetSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class InteriorAtticSummarySerializer(serializers.ModelSerializer):
    """Serializer for interior summary."""

    class Meta:
        model = InteriorAtticSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class BedroomSummarySerializer(serializers.ModelSerializer):
    """Serializer for interior summary."""

    class Meta:
        model = BedroomSummary
        fields = ['id', 'title', 'comment']
        read_only_field = ['id']


class ReceiptInvoiceSerializer(serializers.ModelSerializer):
    """Serializer for receipt invoice model"""

    class Meta:
        model = ReceiptInvoice
        fields = ['report_uuid', 'company', 'date',
                  'inspector_fname', 'inspector_lname',
                  'client_name', 'payment_type', 'total_fee']
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GroundsSerializer(serializers.ModelSerializer):
    """Serializer for grounds model"""

    class Meta:
        model = Grounds
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class RoofSerializer(serializers.ModelSerializer):
    """Serializer for roof model"""

    class Meta:
        model = Roof
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class ExteriorSerializer(serializers.ModelSerializer):
    """Serializer for Exterior model"""

    class Meta:
        model = Exterior
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class GarageCarportSerializer(serializers.ModelSerializer):
    """Garage carport serializer"""

    class Meta:
        model = GarageCarport
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class KitchenSerializer(serializers.ModelSerializer):
    """Kitchen Serializer"""

    class Meta:
        model = Kitchen
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class LaundrySerializer(serializers.ModelSerializer):
    """Laundry Serializer"""

    class Meta:
        model = Laundry
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class BathroomSerializer(serializers.ModelSerializer):
    """Bathroom serializer"""

    class Meta:
        model = Bathroom
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class BedroomSerializer(serializers.ModelSerializer):
    """Bedroom serializer"""

    class Meta:
        model = Bedrooms
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class InteriorSerializer(serializers.ModelSerializer):
    """Interior Serializer"""

    class Meta:
        model = Interior
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class BasementSerializer(serializers.ModelSerializer):
    """Basement Serializer"""

    class Meta:
        model = Basement
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class CrawlspaceSerializer(serializers.ModelSerializer):
    """Crawl Space Serializer"""

    class Meta:
        model = Crawlspace
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class PlumbingSerializer(serializers.ModelSerializer):
    """Plumbing Serializer"""

    class Meta:
        model = Plumbing
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class HeatingSystemSerializer(serializers.ModelSerializer):
    """Heating System Serializer"""

    class Meta:
        model = HeatingSystem
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class ElectricalCoolingSystemsSerializer(serializers.ModelSerializer):
    """Electrical and cooling systems serializer"""

    class Meta:
        model = ElectricalCoolingSystems
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class LivingRoomSerializer(serializers.ModelSerializer):
    """Living Room Serializer"""

    class Meta:
        model = LivingRoom
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}


class DiningRoomSerializer(serializers.ModelSerializer):
    """Dining Room Serializer"""

    class Meta:
        model = DiningRoom
        fields = '__all__'
        read_only_fields = ['user']
        extra_kwargs = {'report_uuid': {'required': 'True'}}
