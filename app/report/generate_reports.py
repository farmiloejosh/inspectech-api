from core.models import *


def get_reports(report_uuid):
    reports = []

    # get reports for all sections and sadd them to our reports list
    reports.extend(get_section_report(report_uuid, Roof))
    reports.extend(get_section_report(report_uuid, Grounds))
    reports.extend(get_section_report(report_uuid, Exterior))
    reports.extend(get_section_report(report_uuid, GarageCarport))
    reports.extend(get_section_report(report_uuid, Kitchen))
    reports.extend(get_section_report(report_uuid, Laundry))
    reports.extend(get_section_report(report_uuid, Bathroom))
    reports.extend(get_section_report(report_uuid, Bedrooms))
    reports.extend(get_section_report(report_uuid, Interior))
    reports.extend(get_section_report(report_uuid, Basement))
    reports.extend(get_section_report(report_uuid, Crawlspace))
    reports.extend(get_section_report(report_uuid, Plumbing))
    reports.extend(get_section_report(report_uuid, HeatingSystem))
    reports.extend(get_section_report(report_uuid, ElectricalCoolingSystems))
    reports.extend(get_section_report(report_uuid, LivingRoom))
    reports.extend(get_section_report(report_uuid, DiningRoom))

    return reports


def get_photos(report_uuid, photosModel):
    photos = []

    if not photosModel:
        return photos

    photosModel = photosModel.objects.filter(report_uuid=report_uuid).all()

    for photo in photosModel:
        photos_value = None
        for attr_name in dir(photo):
            if 'photos' in attr_name:
                photos_value = getattr(photo, attr_name, None)
                break
        if photos_value:
            photos.append(photos_value.url)

    return photos


def get_report_data(itemsModel, itemsField):
    photo_and_comments_uuids = getattr(itemsModel, f'{itemsField}_photo_uuid', [])
    general_comments = getattr(itemsModel, f'{itemsField}_comments', [])

    photo_and_comments = []

    # here we extract, the photos and their specific comments
    for photo_and_comments_uuid in photo_and_comments_uuids:
        try:
            photo_and_comment = PhotoAndComments.objects.filter(uuid=photo_and_comments_uuid).first()
            if photo_and_comment:
                photo_and_comments.append({
                    'photo': photo_and_comment.photo.url,
                    'comments': photo_and_comment.comments,
                })
        except Exception:
            pass

    if not general_comments: general_comments = []
    if not photo_and_comments: photo_and_comments = []

    return {
        'name': itemsField,
        'items': getattr(itemsModel, itemsField, []),
        'photo_and_comments': photo_and_comments,
        'general_comments': general_comments,
    }


def get_section_report(report_uuid, sectionModel):
    reports = []
    columns = []
    section_name = sectionModel.__name__
    itemsModel = sectionModel.objects.filter(report_uuid=report_uuid).all()

    if not itemsModel:
        return reports

    # get all columns with condition values and comments and photos
    for col_name in dir(itemsModel[0]):
        if '_photo_uuid' in col_name:
            columns.append(col_name.replace('_photo_uuid', ''))

    for item in itemsModel:
        report_items = []

        for column in columns:
            column_data = get_report_data(item, column)

            report_items.append(column_data)


        report = {
            'name': section_name,
            'items': report_items
        }

        reports.append(report)

    return reports


def get_report_summary(report_uuid):
    summary = {

    }
    report_details = ReportDetails.objects.filter(report_uuid=report_uuid).first()
    report_details_photo = ReportDetailsPhoto.objects.filter(report_uuid=report_uuid).first()

    if not report_details:
        return summary

    if report_details_photo:
        summary['photo'] = report_details_photo.report_details_photo.url

    summary['report_details'] = report_details
    return summary
